package tests;

import java.sql.SQLException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import src.BusinessLogic;
import src.Credentials;



public class delPkgTest {

    @Test
    public void delRoleTest() throws SQLException {
        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
   
        try {
            Assertions.assertEquals(1, bl.delRole(1,2,1));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void delMarketTest() throws SQLException {
        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        try {
            Assertions.assertEquals(1, bl.delMarket(6));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

     @Test
    public void delRecordLabelTest() throws SQLException {
        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        try {
            Assertions.assertEquals(-1, bl.delRecordLabel(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void delContributorTest() throws SQLException {

        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        try {
            Assertions.assertEquals(1, bl.delContributor(2));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    
    @Test
    public void delDistributionTest() throws SQLException {

        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        try {
            Assertions.assertEquals(1, bl.delDistribution(1) );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void delPerformanceTest() throws SQLException {
        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        try {
            Assertions.assertEquals(1, bl.delSinglePerformance(21));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
