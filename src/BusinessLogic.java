

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Handles all the business logic of the program
 */
public class BusinessLogic {
    private Connection connection;

    public BusinessLogic(String userName, String password) throws SQLException {
        if (userName == null || password == null) {
            throw new IllegalArgumentException();
        }
        
        // setting connection
        this.connection = DriverManager.getConnection(
                "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca", 
                userName,
                password);
        System.out.println("Connected!");
    }

    public BusinessLogic(Credentials credentials) throws SQLException {
        this(credentials.getDBUser(), credentials.getDBPwd());
    }



    /**
     * Returns a string with all the distributions
     * @return String
     * @throws SQLException
     * @author Adam Atamnia
     */
    public String getAllDistributions() throws SQLException{
        String call = "begin ? := GetData.getAllDistributions; end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);

        StringBuilder builder = new StringBuilder();

        while (rs.next()) {
            builder.append("| "+rs.getInt("distId")+" | ");
            builder.append(rs.getString("distname")+" | ");
            builder.append(rs.getDate("releaseDate").toString()+" | ");
            builder.append(rs.getString("typeName")+" | ");
            builder.append(rs.getString("reclabelName")+" | ");
            builder.append(rs.getString("marketName")+" |\n");
        } if(builder.length() == 0) {
            throw new IllegalArgumentException("No distributions exist in this database");
        }

        builder.insert(0, "| Id | Name | Release Date | Type | Record Label | Market |\n");

        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return builder.toString();
    }

    /**
     * Returns a string with all the performances
     * @return String
     * @throws SQLException
     * @author Adam Atamnia
     */
    public String getAllPerformances() throws SQLException{
        String call = "begin ? := GetData.getAllPerformances; end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);

        StringBuilder builder = new StringBuilder();

        while (rs.next()) {
            builder.append("| "+rs.getInt("performId")+" | ");
            builder.append(rs.getString("performName")+" | ");
            builder.append(rs.getDate("createDate").toString()+" | ");
            builder.append(rs.getString("duration")+" |\n");
        } if(builder.length() == 0) {
            throw new IllegalArgumentException("No performances exist in this database");
        }

        builder.insert(0, "| Id | Name | Creation Date | Duration |\n");

        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return builder.toString();
    }

    /**
     * Returns a string with all the contributors
     * @return String
     * @throws SQLException
     * @author Adam Atamnia
     */
    public String getAllContributorsFromTable() throws SQLException{
        String call = "begin ? := GetData.getAllContributorsFromTable; end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);

        StringBuilder builder = new StringBuilder();

        while (rs.next()) {
            builder.append("| "+rs.getInt("contId")+" | ");
            builder.append(rs.getString("firstname")+" | ");
            builder.append(rs.getString("lastname")+" |\n");
        } if(builder.length() == 0) {
            throw new IllegalArgumentException("No contributors exist in this database");
        }

        builder.insert(0, "| Id | First Name | Last Name |\n");

        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return builder.toString();
    }

    /**
     * Creates a distribution
     * @param distId
     * @return
     * @throws SQLException
     * @author Adam Atamnia
     */
    public Distribution getDistribution(int distId) throws SQLException{
        String call = "begin ? := GetData.getDistributionDetails(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setInt(2, distId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Distribution does not exist");
        }

        Distribution distribution = null;

        if (rs.next()) {
            distribution = new Distribution(
                    distId, 
                    rs.getString("distname"), 
                    rs.getString("typename"), 
                    rs.getDate("releasedate"), 
                    rs.getString("reclabelname"), 
                    rs.getString("marketname"), 
                    getPerformancesFromDistribution(distId));
        } else {
            throw new IllegalArgumentException("Perfomance does not exist");
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return distribution;
    }

    /**
     * Returns a list of the performances in a given distribution
     * @param distId
     * @return List<APerformance>
     * @throws SQLException
     * @author Adam Atamnia
     */
    public List<APerformance> getPerformancesFromDistribution(int distId) throws SQLException{
        String call = "begin ? := GetData.getPerformancesFromDistribution(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setInt(2, distId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Distribution does not exist");
        }

        List<APerformance> performances = new ArrayList<APerformance>();

        while (rs.next()) {
            performances.add(this.getPerformance(rs.getInt("performId")));
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return performances;
    }

    /**
     * Returns a list of the performances a given contributor played a part in
     * Returns an empty list if no performance is found with the given filters (not an error)
     * 
     * @param contId
     * @return List<APerformance>
     * @throws SQLException
     * @author Adam Atamnia
     */
    public List<APerformance> getPerformancesFromContributor(int contId, boolean releasedOnly, boolean notReleasedOnly, boolean compilationOnly, boolean recordingOnly) throws SQLException{
        String call = "begin ? := GetData.getPerformancesFromContributor(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setInt(2, contId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Contributor does not exist");
        }

        if(releasedOnly && notReleasedOnly || compilationOnly && recordingOnly){
            throw new IllegalArgumentException("Filters Cannot be releasedOnly AND notReleasedOnly, OR compilationOnly AND recordingOnly");
        }

        List<APerformance> performances = new ArrayList<APerformance>();

        while (rs.next()) {
            int performId = rs.getInt("performId");
            
            boolean isAddable = true;

            if(!this.isReleased(performId) && releasedOnly){
                isAddable = false;
            
            } else if(this.isReleased(performId) && notReleasedOnly){
                isAddable = false;

            } else if(!this.isCompilation(performId) && compilationOnly){
                isAddable = false;

            } else if(this.isCompilation(performId) && recordingOnly){
                isAddable = false;

            }
            
            if(isAddable){
                performances.add(this.getPerformance(performId));
            }
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return performances;
    }

    /**
     * Returns true or false to the question: is this performance a compilation?
     * @param performId
     * @return boolean
     * @throws SQLException
     * @author Adam Atamnia
     */
    private boolean isCompilation(int performId) throws SQLException{
        String call = "begin ? := GetData.isCompilation(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.NUMERIC); 
        cstmt.setInt(2, performId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Performance does not exist");
        }

        if(cstmt.getInt(1) == 0){
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns true or false to the question: is this performance released?
     * @param performId
     * @return boolean
     * @throws SQLException
     * @author Adam Atamnia
     */
    private boolean isReleased(int performId) throws SQLException{
        String call = "begin ? := GetData.isReleased(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.NUMERIC); 
        cstmt.setInt(2, performId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Performance does not exist");
        }

        if(cstmt.getInt(1) == 0){
            return false;
        } else {
            return true;
        }
    }

    /**
     * Creates a performance (compilation or recording when appropriate)
     * @param performId
     * @return APerformance
     * @throws SQLException
     * @author Adam Atamnia
     */
    public APerformance getPerformance(int performId) throws SQLException{
        String call = "begin ? := GetData.isCompilation(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.NUMERIC); 
        cstmt.setInt(2, performId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Perfomance does not exist");
        }

        boolean isCompilation = cstmt.getInt(1) == 1? true:false;

        APerformance performance = null;

        if(isCompilation){
            performance = getCompilation(performId);
        } else {
            performance = getRecording(performId);
        }
        if (cstmt != null)
            cstmt.close();

        return performance;
    }

    /**
     * Creates a recording
     * @param performId
     * @return APerformance
     * @throws SQLException
     * @author Adam Atamnia
     */
    private APerformance getRecording(int performId) throws SQLException{
        String call = "begin ? := GetData.getPerformanceDetails(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setInt(2, performId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Perfomance does not exist");
        }

        APerformance recording = null;

        if (rs.next()) {
            recording = new Recording(performId, rs.getString("performname"), rs.getDate("createdate"), rs.getString("duration"), getAllContributorsAndRoles(performId));
        } else {
            throw new IllegalArgumentException("Perfomance does not exist");
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return recording;
    }
    
    /**
     * Creates a compilation
     * @param performId
     * @return APerformance
     * @throws SQLException
     * @author Adam Atamnia
     */
    private APerformance getCompilation(int performId) throws SQLException{
        String call = "begin ? := GetData.getPerformanceDetails(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setInt(2, performId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Perfomance does not exist");
        }

        APerformance compilation = null;

        if (rs.next()) {
            compilation = new Compilation(
                    performId, 
                    rs.getString("performname"), 
                    rs.getDate("createdate"), 
                    rs.getString("duration"), 
                    getAllContributorsAndRoles(performId),
                    getAllComponents(performId));
        } else {
            throw new IllegalArgumentException("Perfomance does not exist");
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return compilation;
    }

    /**
     * Creates a list of contributors given a performance
     * @param performId
     * @return List<Contributor>
     * @throws SQLException
     * @author Adam Atamnia
     */
    public List<Contributor> getAllContributorsAndRoles(int performId) throws SQLException{
        String call = "begin ? := GetData.getAllContributors(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setInt(2, performId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Perfomance does not exist");
        }

        List<Contributor> contributors = new ArrayList<Contributor>();
        
        while(rs.next()) {
            
            contributors.add(new Contributor(
                rs.getInt("contId"), 
                rs.getString("firstname")+" "+rs.getString("lastname"), 
                this.getContributorRolesFromPerformance(performId, rs.getInt("contId"))));
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return contributors;
    }

    /**
     * Returns a list of strings of all the roles of a contributor
     * @param contId
     * @return List<String>
     * @throws SQLException
     * @author Adam Atamnia
     */
    public List<String> getAllRoles(int contId) throws SQLException{
        String call = "begin ? := GetData.getAllRoles(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setInt(2, contId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Contributor does not exist");
        }

        List<String> roles = new ArrayList<String>();
        
        while(rs.next()) {
            roles.add(rs.getString("rolename"));
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return roles;
    }

    /**
     * Returns a list of strings of all the roles of a contributor for a given performance
     * @param performId
     * @param contId
     * @return List<String>
     * @throws SQLException
     * @author Adam Atamnia
     */
    public List<String> getContributorRolesFromPerformance(int performId, int contId) throws SQLException{
        String call = "begin ? := GetData.getContributorRolesFromPerformance(?,?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setInt(2, performId); 
        cstmt.setInt(3, contId);
        cstmt.registerOutParameter(4, Types.NUMERIC); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);
        if(cstmt.getInt(4) == 0){
            throw new IllegalArgumentException("Perfomance or contributor does not exist");
        }

        List<String> roles = new ArrayList<String>();
        
        while(rs.next()) {
            roles.add(rs.getString("rolename"));
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return roles;
    }

    /**
     * Creates a list of direct components given a performance
     * @param performId
     * @return List<Component>
     * @throws SQLException
     * @author Adam Atamnia
     */
    public List<Component> getAllComponents(int performId) throws SQLException{
        String call = "begin ? := GetData.getAllComponents(?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setInt(2, performId); 
        cstmt.registerOutParameter(3, Types.NUMERIC); 
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);
        if(cstmt.getInt(3) == 0){
            throw new IllegalArgumentException("Perfomance does not exist");
        }

        List<Component> components = new ArrayList<Component>();

        while (rs.next()) {
            components.add(new Component(
                getPerformance(rs.getInt("componentId")), 
                rs.getString("offsetFromBeginning"), 
                rs.getString("offsetFromSelf"), 
                rs.getString("durationUsed")));
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return components;
    }

    /**
     * Returns a list of activity logs between the specified time
     * @param startDate
     * @param endDate
     * @return List<ActivityLog>
     * @throws SQLException
     * @author Adam Atamnia
     */
    public List<ActivityLog> getActivityLogs(Date startDate, Date endDate) throws SQLException{
        if(startDate == null || endDate == null){
            throw new IllegalArgumentException("Date Cannot be null");
        }

        String call = "begin ? := GetData.getActivityLogs(?,?,?); end;";
        CallableStatement cstmt = this.connection.prepareCall(call);
        cstmt.registerOutParameter(1, Types.REF_CURSOR); 
        cstmt.setDate(2, startDate); 
        cstmt.setDate(3, endDate);
        cstmt.registerOutParameter(4, Types.NUMERIC);
        cstmt.executeUpdate();
        ResultSet rs = (ResultSet)cstmt.getObject(1);

        if(cstmt.getInt(4) == 0){
            throw new IllegalArgumentException("Start date cannot be before or equal to end date");
        }
        List<ActivityLog> activityLogs = new ArrayList<ActivityLog>();

        while (rs.next()) {
            activityLogs.add(new ActivityLog(
                rs.getInt("logid"), 
                rs.getString("username"), 
                rs.getString("typechange"), 
                rs.getString("changedtable"), 
                rs.getString("message"), 
                rs.getDate("datetime")));
        }
        if(activityLogs.size() == 0){
            throw new IllegalArgumentException("No activity log found in that time range");
        }
        if (cstmt!= null)
            cstmt.close();
        if (rs!= null)
            rs.close();

        return activityLogs;
    }


    /**
     * Closes the connection with our database
     * @throws SQLException
     * @author Adam Atamnia
     */
    public void close() throws SQLException{
        if(this.connection != null)
            this.connection.close();
        
    }

    
    /** 
     * updates Marketname based on the id
     * @param id
     * @param newMarketName
     * @return String
     * @throws SQLException
     */
    public String updateMarket(int id,String newMarketName) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_market(?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setString(2, newMarketName);
        cstmt.registerOutParameter(3, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(3);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }
    
    /** 
     * updates a distribution based on a distribution id
     * @param id
     * @param newName
     * @param newType
     * @param newReleaseDate
     * @param newRecLabelId
     * @param newMarketId
     * @return String
     * @throws SQLException
     */
    public String updateDistribution(int id,String newName,int newType,Date newReleaseDate,int newRecLabelId,int newMarketId) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_distribution(?,?,?,?,?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setString(2, newName);
        cstmt.setInt(3, newType);
        cstmt.setDate(4, newReleaseDate);
        cstmt.setInt(5, newRecLabelId);
        cstmt.setInt(6, newMarketId);
        cstmt.registerOutParameter(7, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(7);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }
    
    /** 
     * updates a contributor based on a contributor id
     * @param id
     * @param newFirstName
     * @param newLastName
     * @return String
     * @throws SQLException
     */
    public String updateContributor(int id,String newFirstName,String newLastName) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_contributor(?,?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setString(2, newFirstName);
        cstmt.setString(3, newLastName);
        cstmt.registerOutParameter(4, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(4);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }
    
    /** 
     * updates a entry to distribution_performance based on a distribution id
     * @param id
     * @param newPerformId
     * @return String
     * @throws SQLException
     */
    public String updateDistributionPerformance(int id,int newPerformId) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_distribution_performance(?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setInt(2, newPerformId);
        cstmt.registerOutParameter(3, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(3);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }
    
    /** 
     * updates a distribution type based on a distribution id
     * @param id
     * @param newTypeName
     * @return String
     * @throws SQLException
     */
    public String updateDistributionTypes(int id,String newTypeName) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_distributiontypes(?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setString(2, newTypeName);
        cstmt.registerOutParameter(3, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(3);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }
    
    /** 
     * updates a performanc based on a performance id
     * @param id
     * @param newPerformanceName
     * @param newCreateDate
     * @param newDuration
     * @return String
     * @throws SQLException
     */
    public String updatePerformance(int id,String newPerformanceName,Date newCreateDate,String newDuration) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_performance(?,?,?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setString(2, newPerformanceName);
        cstmt.setDate(3, newCreateDate);
        cstmt.setString(4, newDuration);
        cstmt.registerOutParameter(5, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(5);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }
    
    /** 
     * updates a entry of performance_conponent based on a performance id
     * @param id
     * @param newConponentId
     * @param newOffsetFromBeginning
     * @param newOffsetFromSelf
     * @param newDurationUsed
     * @return String
     * @throws SQLException
     */
    public String updatePerformanceComponent(int id,int newConponentId,String newOffsetFromBeginning,String newOffsetFromSelf,String newDurationUsed) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_performance_component(?,?,?,?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setInt(2, newConponentId);
        cstmt.setString(3, newOffsetFromBeginning);
        cstmt.setString(4, newOffsetFromSelf);
        cstmt.setString(5, newDurationUsed);
        cstmt.registerOutParameter(6, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(6);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }
    
    /** 
     * update an entry of performance_contributor_role based on perfonmance id
     * @param id
     * @param newContId
     * @param newRoleId
     * @return String
     * @throws SQLException
     */
    public String updatePerformanceContributorRole(int id,int newContId,int newRoleId) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_PERFORMANCE_CONTRIBUTOR_ROLE(?,?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setInt(2, newContId);
        cstmt.setInt(3, newRoleId);
        cstmt.registerOutParameter(4, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(4);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }
    
    /** 
     * update a record label name based on a record label id
     * @param id
     * @param newName
     * @return String
     * @throws SQLException
     */
    public String updateRecordLabel(int id,String newName) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_recordlabel(?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setString(2, newName);
        cstmt.registerOutParameter(3, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(3);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }
    
    /** 
     * update role name based on a role id
     * @param id
     * @param newRoleName
     * @return String
     * @throws SQLException
     */
    public String updateRole(int id,String newRoleName) throws SQLException{
        String wasItASuccess;
        String sql="call all_updates.update_role(?,?,?)";
        CallableStatement cstmt=connection.prepareCall(sql);
        
        cstmt.setInt(1, id);
        cstmt.setString(2, newRoleName);
        cstmt.registerOutParameter(3, Types.VARCHAR);

        cstmt.execute();
        wasItASuccess=cstmt.getString(3);

        if (wasItASuccess.equals("1")){
            return "the Marketr was successfully updated";
        }else{
            return "the Market has failed to update";
        }
    }

    /**
     * Closes The connection when garbage collector disposes of this object
     * @author Adam Atamnia
     */
    @Override
    protected void finalize() {
        try {
            this.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


     /**
     * Adds a new Role based on the given parameters.
     * @author Cuneyt YILDIRIM
     * @param roleName
     * @return String value and gives result of execution 1 represents success, 0 is not success. 
     * @throws SQLException
     */
    public int addRole(String roleName) throws SQLException {
        String putQuery = "call AddProPkg.addRole(?,?)";
        CallableStatement tStatement = this.connection.prepareCall(putQuery);
        tStatement.setString(1, roleName);
        tStatement.registerOutParameter(2, Types.NUMERIC);

        tStatement.executeUpdate();
        int res = tStatement.getInt(2);
        if( res == 0 ){
            throw new IllegalArgumentException("role exists in db");
        }
        tStatement.close();
        return res;
    }

    /**
     * Adds a new Record Label based on the given parameters.
     * @author Cuneyt YILDIRIM
     * @param recordName
     * @return String value and gives result of execution 1 represents success, 0 is not success. 
     * @throws SQLException
     */
    public int addRecordLabel(String recordName) throws SQLException{
        String addRecordLabel = "call AddProPkg.addRecordLabel(?,?)";
        CallableStatement tStatement = this.connection.prepareCall(addRecordLabel);
        tStatement.setString(1, recordName);
        tStatement.registerOutParameter(2, Types.NUMERIC);
        tStatement.executeUpdate();

        int result = tStatement.getInt(2);
        if( result== 0){
            throw new IllegalArgumentException("Record Label is already in database.");
        }
       tStatement.close();
       return result;
    }

    /**
     * Adds a new Market in databases based on the given parameters.
     * @author Cuneyt YILDIRIM
     * @param marketName
     * @return String value and gives result of execution 1 represents success, 0 is not success. 
     * @throws SQLException
     */
    public int addMarket(String marketName) throws SQLException{
        String addMarketLabel = "call AddProPkg.addMarket(?,?)";
        CallableStatement tStatement = this.connection.prepareCall(addMarketLabel);
        tStatement.setString(1, marketName);
        tStatement.registerOutParameter(2, Types.NUMERIC);
        tStatement.executeUpdate();

        int result = tStatement.getInt(2);
        if(result== 0){
            throw new IllegalArgumentException("Market does not exist.");
        }
        tStatement.close();
        return result;
    
    }

    /**
     * Adds a Distribution Type in database based on the given parameters
     * @author Cuneyt YILDIRIM.
     * @param typeName
     * @return String value and gives result of execution 1 represents success, 0 is not success. 
     * @throws SQLException
     */
    public int addDistributionType(String typeName) throws SQLException{

        String addDistributionType = "call AddProPkg.addDistributionType(?,?)";
        CallableStatement tStatement = this.connection.prepareCall(addDistributionType);
        tStatement.setString(1, typeName);
        tStatement.registerOutParameter(2, Types.NUMERIC);
        tStatement.executeUpdate();

        int result = tStatement.getInt(2);
        if(result == 0){
            throw new IllegalArgumentException("Distribution Type is already in database.");
        }
        tStatement.close();
        return result;
    }

    /**
     * Adds a Contributor in database based on the given parameters.
     * @author Cuneyt YILDIRIM
     * @param firstname
     * @param lastName
     * @return String value and gives result of execution 1 represents success, 0 is not success. 
     * @throws SQLException
     */
    public int addContributor(String firstname, String lastName) throws SQLException{
        String addDistributor = "call AddProPkg.addContributor(?,?,?)";
        CallableStatement tStatement = this.connection.prepareCall(addDistributor);
        tStatement.setString(1, firstname);
        tStatement.setString(2, lastName);
        tStatement.registerOutParameter(3, Types.NUMERIC);
        tStatement.executeUpdate();
        int result = tStatement.getInt(3);
        if (result== 0) {
            throw new IllegalArgumentException("Contributor is alreay in database.");
        }
        tStatement.close();
        return result;
        
    }

    /**
     * Adds a Distribution in database based on the given parameters.
     * @author Cuneyt YILDIRIM
     * @param distName
     * @param distTypeId
     * @param releaseDate
     * @param recLabelId
     * @param marketId
     * @return String value and gives result of execution 1 represents success, 0 is not success. 
     * @throws SQLException
     */
    public int addDistribution(String distName, int distTypeId, Date releaseDate, int recLabelId, int marketId) throws SQLException{
        String distrtbn = "call  AddProPkg.addDistribution(?,?,?,?,?,?)";
        CallableStatement tStatement = this.connection.prepareCall(distrtbn);
        tStatement.setString(1, distName);
        tStatement.setDate(3, releaseDate);
        tStatement.setInt(2, distTypeId);
        tStatement.setInt(4, recLabelId);
        tStatement.setInt(5, marketId);        
        
        tStatement.registerOutParameter(6, Types.NUMERIC);
        tStatement.executeUpdate();
        int result = tStatement.getInt(6);
        if(result== 0){
            throw new IllegalArgumentException("Contributor is already in database.");
        }
        tStatement.close();
        return result;
    }

        /**
     * Adds a single performance without components based on the given parameters.
     * @author Cuneyt YILDIRIM
     * @param performName
     * @param createDate
     * @param duration
     * @return String value and gives result of execution 1 represents success, 0 is not success.     
     * @throws SQLException
     */
    public int addSinglePerformance(String performName,Date createDate, String duration) throws SQLException{
        String perform ="call AddProPkg.addSinglePerformance(?,?,?,?)";
        CallableStatement tStatement = this.connection.prepareCall(perform);
        tStatement.setString(1, performName);
        tStatement.setDate(2, createDate);
        tStatement.setString(3, duration);
        tStatement.registerOutParameter(4, Types.NUMERIC);
        
        tStatement.executeUpdate();
        int result = tStatement.getInt(4);
        if(result == 0){
            throw new IllegalArgumentException("Contributor is already in database.");
        }
        tStatement.close();
        return result;
    }

        /**
         * Deletes a role in database with related table.
         * @author Cuneyt YILDIRIM
         * @param roleid
         * @param performid
         * @param contid
         * @return String value and gives result of execution 1 represents success, 0 is not success. 
         * @throws SQLException
         */
        public int delRole(int roleid, int performid, int contid) throws SQLException {
            String putQuery = "call delProPkg.delRole(?,?,?,?)";
            CallableStatement tStatement = this.connection.prepareCall(putQuery);
            tStatement.setInt(1, roleid);
            tStatement.setInt(2, performid);
            tStatement.setInt(3, contid);
            tStatement.registerOutParameter(4, Types.NUMERIC);
    
            tStatement.executeUpdate();
            int res = tStatement.getInt(4);
            if( res == 0 ){
                throw new Error("role does not exist in database.");
            }
            tStatement.close();
            return res;
        }
    
        /**
         * Deletes records in database based on the Record Label id.
         * @author Cuneyt YILDIRIM
         * @param recordId
         * @return String value and gives result of execution 1 represents success, 0 is not success. 
         * @throws SQLException
         */
        public int delRecordLabel(int recordId) throws SQLException{
            String recLabel = "call delProPkg.delRecordLabel(?,?)";
            CallableStatement tStatement = this.connection.prepareCall(recLabel);
            tStatement.setInt(1, recordId);
            tStatement.registerOutParameter(2, Types.NUMERIC);
            tStatement.executeUpdate();
    
            int result = tStatement.getInt(2);
            if(result== 0){
                throw new IllegalArgumentException("Record Label does not exist in database..");
            }
           tStatement.close();
           return result;
        }
    
        /**
         * Deletes a markets in database based on the market id
         * @author Cuneyt YILDIRIM
         * @param marketId
         * @return String value and gives result of execution 1 represents success, 0 is not success. 
         * @throws SQLException
         */
        public int delMarket(int marketId) throws SQLException{
            String market = "call delProPkg.delMarket(?,?)";
            CallableStatement tStatement = this.connection.prepareCall(market);
            tStatement.setInt(1, marketId);
            tStatement.registerOutParameter(2, Types.NUMERIC);
            tStatement.executeUpdate();
            int result = tStatement.getInt(2);
            System.out.println(result);
            if(result == 0){
                throw new IllegalArgumentException("Market does not exist in database.");
            }else if( result== -1){
                throw new IllegalArgumentException("Before deleting the market, please delete related distributions");
            }
            tStatement.close();
            return result;
        
        }
            /**
         * Deletes a Distribution Types from database based on the distribution type id.
         * @author Cuneyt YILDIRIM
         * @param typeName
         * @return String value and gives result of execution 1 represents success, 0 is not success. 
         * @throws SQLException
         */
        public int delDistributionType(int distId) throws SQLException{
    
            String addDistributionType = "call delProPkg.delDistributionType(?,?)";
            CallableStatement tStatement = this.connection.prepareCall(addDistributionType);
            tStatement.setInt(1, distId);
            tStatement.registerOutParameter(2, Types.NUMERIC);
            tStatement.executeUpdate();
    
            int result =tStatement.getInt(2);
            if(result == 0){
                throw new IllegalArgumentException("Distribution Type does not exist in database.");
            }else if( result ==-1 ){
                throw new IllegalArgumentException("Before deleting the distribution type, please delete related distributions");
            }
       
            tStatement.close();
            return result;
        }
    
        /**
         * Deletes a contributor in the database based on provided Contributor id.
         * @author Cuneyt YILDIRIM
         * @param p_contid
         * @return String value and gives result of execution 1 represents success, 0 is not success. 
         * @throws SQLException
         */
        public int delContributor(int p_contid) throws SQLException{
            String contributor = "call delProPkg.delContributor(?,?)";
            CallableStatement tStatement = this.connection.prepareCall(contributor);
            tStatement.setInt(1, p_contid);
            tStatement.registerOutParameter(2, Types.NUMERIC);
            tStatement.executeUpdate();
            int result = tStatement.getInt(2);
            if (result == 0) {
                throw new IllegalArgumentException("Contributor does not exist in database.");
            }
            tStatement.close();
            return result;
            
        }
    
    
        /**
         * Deletes a Distribution in the database based on the provided Distribution id.
     * @author Cuneyt YILDIRIM
         * @param p_distid
         * @return String value and gives result of execution 1 represents success, 0 is not success. 
         * @throws SQLException
         */
    
        public int delDistribution(int p_distid) throws SQLException{
            String distrtbn = "call delProPkg.delDistribution(?,?)";
            CallableStatement tStatement = this.connection.prepareCall(distrtbn);
            tStatement.setInt(1, p_distid);
            tStatement.registerOutParameter(2, Types.NUMERIC);
            tStatement.executeUpdate();
            int result = tStatement.getInt(2);
            if(result == 0 ){
                throw new IllegalArgumentException("Contributor does not exist in database.");
            }
            tStatement.close();
            return result;
        }
    
        /**
         * Deletes a single Performance based on the given Performance id.
         * @author Cuneyt YILDIRIM
         * @param performid
         * @return String value and gives result of execution 1 represents success, 0 is not success. 
         * @throws SQLException
         */
        public int delSinglePerformance(int performid) throws SQLException{
            String perform ="call delProPkg.delSinglePerformance(?,?)";
            CallableStatement tStatement = this.connection.prepareCall(perform);
            tStatement.setInt(1, performid);
            tStatement.registerOutParameter(2, Types.NUMERIC);
            
            tStatement.executeUpdate();
            int result = tStatement.getInt(2);
            if(result == 0 ){
                throw new IllegalArgumentException("Performance does not exist in database.");
            }
            tStatement.close();
            return result;
        }

        /**
         * Deletes a Component based on the given Component id.
         * @author Cuneyt YILDIRIM
         * @param performid
         * @return String value and gives result of execution 1 represents success, 0 is not success. 
         * @throws SQLException
         */
        public int delComponent(String performid) throws SQLException{
            String component ="call delPackage.addSinglePerformance(?,?)";
            CallableStatement tStatement = this.connection.prepareCall(component);
            tStatement.setString(1, performid);
            tStatement.registerOutParameter(2, Types.NUMERIC);
            
            tStatement.executeUpdate();
            int result = tStatement.getInt(2);
            if(result == 0){
                throw new IllegalArgumentException("Component is already in database.");
            }
    
            tStatement.close();
            return result;
        }

    /**
     * Adds a performance without components based on the given parameters.
     * @author Cuneyt YILDIRIM
     * @param performName
     * @param createDate
     * @param duration
     * @return String value and gives result of execution 1 represents success, 0 is not success.     
     * @throws SQLException
     */
    public int addPerformanceComponents(int compilationId, int componentId, String offSetBegin, String offSetSelf, String duration) throws SQLException{
        String perform ="call AddProPkg.addComponent(?,?,?,?,?,?)";
        CallableStatement tStatement = this.connection.prepareCall(perform);
        tStatement.setInt(1, compilationId);
        tStatement.setInt(2, componentId);
        tStatement.setString(3, offSetBegin);
        tStatement.setString(4, offSetSelf);
        tStatement.setString(5, duration);
        tStatement.registerOutParameter(6, Types.NUMERIC);
        
        tStatement.executeUpdate();
        int result = tStatement.getInt(6);
        if(result == 0){
            throw new IllegalArgumentException("Component is already in database.");
        }
        tStatement.close();
        return result;
    }

}
