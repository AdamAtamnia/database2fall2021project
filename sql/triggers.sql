/*create a procedure to insert a log activity*/
create or replace procedure insertLogin
(type_change in activitylog.typechange%type,
changed_table in activitylog.changedtable%type,
msg in activitylog.message%type)
as
    id activitylog.logid%type;
    usr activitylog.username%type;
begin
    select user into usr from dual;
    select MAX(logid) into id from activitylog;
    if id is null then
        id:=1;
    else
        id:=id+1;
    end if;
    insert into activitylog values(id,usr,type_change,changed_table,msg,SYSDATE);
end;
/
/*trigger to check for insert delete or update on the performance table*/
create or replace trigger performance_trigger
after INSERT or UPDATE or DELETE
on PERFORMANCE
for each row
DECLARE
    msg activitylog.message%type;
    usr varchar2(50);
    changeType ACTIVITYLOG.typechange%type;
    changed_col PERFORMANCE.performid%type;
begin
    select user into usr from dual;
    changed_col:= :OLD.performid;
    if inserting then
        changeType:='INSERTED';
        changed_col:= :NEW.performid;
    ELSIF updating THEN
        changeType:='UPDATED';
    ELSE
        changeType:='DELETED';
    END IF;
    msg:='user '||usr|| ' ' || changeType ||' perform id '|| changed_col ||'';
    insertLogin(changeType,'PERFORMANCE',msg);
end;
/

/*trigger to check for update, insert or delete on distribution*/
create or replace trigger distribution_trigger
after INSERT or UPDATE or DELETE
on DISTRIBUTION
for each row
DECLARE
    usr activitylog.username%type;
    msg activitylog.message%type;
    changeType ACTIVITYLOG.typechange%type;
    changed_col DISTRIBUTION.distid%type;
begin
    select user into usr from dual;
    changed_col:= :OLD.distid;
    if inserting then
        changeType:='INSERT';
        changed_col:= :NEW.distid;
    ELSIF updating THEN
        changeType:='UPDATE';
    ELSE
        changeType:='DELETE';
    END IF;
    msg:='user '||usr|| ' ' || changeType ||' distribution id '|| changed_col ||'';
    insertLogin(changeType,'DISTRIBUTION',msg);
end;
/

/*trigger for contributor*/
create or replace trigger contributor_trigger
after INSERT or UPDATE or DELETE
on CONTRIBUTOR
for each row
DECLARE
    changed_col CONTRIBUTOR.CONTID%type;
    usr activitylog.username%type;
    msg activitylog.message%type;
    changeType ACTIVITYLOG.typechange%type;
begin
    select user into usr from dual;
    changed_col:= :OLD.contid;
    if inserting then
        changeType:='INSERT';
        changed_col:= :NEW.contid;
    ELSIF updating THEN
        changeType:='UPDATE';
    ELSE
        changeType:='DELETE';
    END IF;
    msg:='user '||usr|| ' ' || changeType ||' contributor name '|| changed_col ||'';
    insertLogin(changeType,'CONTRIBUTOR',msg);
end;
/

/*trigger for role*/
create or replace trigger role_trigger
after INSERT or UPDATE or DELETE
on ROLE
for each row
DECLARE
    changed_col ROLE.roleid%type;
    usr activitylog.username%type;
    msg activitylog.message%type;
    changeType ACTIVITYLOG.typechange%type;
begin
    select user into usr from dual;
    changed_col:= :OLD.roleid;
    if inserting then
        changeType:='INSERT';
        changed_col:= :NEW.roleid;
    ELSIF updating THEN
        changeType:='UPDATE';
    ELSE
        changeType:='DELETE';
    END IF;
    msg:='user '||usr|| ' ' || changeType ||' role id '|| changed_col ||'';
    insertLogin(changeType,'ROLE',msg);
end;
/

/*trigger for recordlabel*/
create or replace trigger recordlabel_trigger
after INSERT or UPDATE or DELETE
on RECORDLABEL
for each row
DECLARE
    changed_col RECORDLABEL.RECLABELID%type;
    usr activitylog.username%type;
    msg activitylog.message%type;
    changeType ACTIVITYLOG.typechange%type;
begin
    select user into usr from dual;
    changed_col:= :OLD.reclabelid;
    if inserting then
        changeType:='INSERT';
        changed_col:= :NEW.reclabelid;
    ELSIF updating THEN
        changeType:='UPDATE';
    ELSE
        changeType:='DELETE';
    END IF;
    msg:='user '||usr|| ' ' || changeType ||' record label id '|| changed_col ||'';
    insertLogin(changeType,'RECORDLABEL',msg);
end;
/

/*trigger for market*/
create or replace trigger market_trigger
after INSERT or UPDATE or DELETE
on MARKET
for each row
DECLARE
    changed_col MARKET.MARKETID%type;
    usr activitylog.username%type;
    msg activitylog.message%type;
    changeType ACTIVITYLOG.typechange%type;
begin
    select user into usr from dual;
    changed_col:= :OLD.marketid;
    if inserting then
        changeType:='INSERT';
        changed_col:= :NEW.marketid;
    ELSIF updating THEN
        changeType:='UPDATE';
    ELSE
        changeType:='DELETE';
    END IF;
    msg:='user '||usr|| ' ' || changeType ||' market id '|| changed_col ||'';
    insertLogin(changeType,'MARKET',msg);
end;
/


