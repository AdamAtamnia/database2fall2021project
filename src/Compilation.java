

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a compilation (It has components)
 * @author Adam Atamnia
 */
public class Compilation extends APerformance{

    private List<Component> components;

    public Compilation(int performId, String name, Date creationDate, String duration, List<Contributor> contributors, List<Component> components){
        super(performId, name, creationDate, duration, contributors);

        if (components == null) {
            throw new IllegalArgumentException();
        }
        
        this.components = new ArrayList<Component>(components);
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;

        if(!(obj instanceof Compilation)){
            return false;
        }

        Compilation compilation = (Compilation)obj;
        if (
            super.equals(obj) &&
            this.components.equals(compilation.components)
            ) {
            isEqual = true;
        }
        return isEqual;
    }

    @Override
    public String toString() {
        
        StringBuilder builder = new StringBuilder(super.toString()+"\n");
        builder.append("Components of "+this.getName()+":\n");
        builder.append("| Offset Parent | Offset Self | Duration Used |\n");
        for (Component component : components) {
            builder.append("---------------------------Component of "+this.getName()+"-------------------------------\n");
            builder.append(component+"\n");
        } 
        return builder.toString();
    }

}
