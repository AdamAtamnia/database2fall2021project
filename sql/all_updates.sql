create or replace package all_updates AS
    procedure check_if_exists(where_value IN number,table_name IN varchar2,col_name IN varchar2,checker OUT number);
    procedure update_distribution(id IN DISTRIBUTION.distid%type,name IN DISTRIBUTION.distname%type,typeId IN DISTRIBUTION.disttypeid%type,release IN DISTRIBUTION.releasedate%type,record IN DISTRIBUTION.reclabelid%type,market IN DISTRIBUTION.marketid%type,results OUT varchar2);
    procedure update_contributor(id IN CONTRIBUTOR.contid%type,fname IN CONTRIBUTOR.firstname%type,lname IN CONTRIBUTOR.lastname%type,results OUT varchar2);
    procedure update_distribution_performance(dist_id IN DISTRIBUTION_PERFORMANCE.distid%type,perform_id IN DISTRIBUTION_PERFORMANCE.performid%type,results OUT varchar2);
    procedure update_distributiontypes(dist_type_id IN DISTRIBUTIONTYPES.disttypeid%type,type_name IN DISTRIBUTIONTYPES.typename%type,results OUT varchar2);
    procedure update_market(id IN MARKET.marketid%type,name IN MARKET.marketname%type,results OUT varchar2);
    procedure update_performance(id IN PERFORMANCE.performid%type,name IN PERFORMANCE.performname%type,create_date IN PERFORMANCE.createdate%type,dur IN PERFORMANCE.duration%type,results OUT varchar2);
    procedure update_performance_component(compilation_id IN PERFORMANCE_COMPONENt.compilationid%type,component_id IN PERFORMANCE_COMPONENT.componentid%type,offset_from_start IN PERFORMANCE_COMPONENT.offsetfrombeginning%type,offset_from_self IN PERFORMANCE_COMPONENT.offsetfromself%type,duration_used IN PERFORMANCE_COMPONENT.durationused%type,results OUT varchar2);
    procedure update_PERFORMANCE_CONTRIBUTOR_ROLE(perform_id IN PERFORMANCE_CONTRIBUTOR_ROLE.performid%type,cont_id IN PERFORMANCE_CONTRIBUTOR_ROLE.contid%type,role_id IN PERFORMANCE_CONTRIBUTOR_ROLE.roleid%type,results OUT varchar2);
    procedure update_recordlabel(id IN RECORDLABEL.reclabelid%type,name IN RECORDLABEL.reclabelname%type,results OUT varchar2);
    procedure update_role(id in ROLE.ROLEID%TYPE,name IN role.rolename%type,results OUT varchar2);
END all_updates;
/
create or replace PACKAGE BODY all_updates AS
    procedure check_if_exists(
    where_value IN number,
    table_name IN varchar2,
    col_name IN varchar2,
    checker OUT number
)
AS
    update_result number;
    sql_string varchar2(500);
BEGIN
    checker:=1;
    sql_string:='select ' || col_name || ' from ' || table_name || ' where ' || col_name ||' = ' ||  where_value ||' FETCH NEXT 1 ROWS ONLY';
    EXECUTE IMMEDIATE sql_string into update_result;
    if SQL%notfound then
        checker:=0;
    end if;
EXCEPTION
    when NO_DATA_FOUND THEN
    checker:=0;
end check_if_exists;

    procedure update_distribution(
    id IN DISTRIBUTION.distid%type,
    name IN DISTRIBUTION.distname%type,
    typeId IN DISTRIBUTION.disttypeid%type,
    release IN DISTRIBUTION.releasedate%type,
    record IN DISTRIBUTION.reclabelid%type,
    market IN DISTRIBUTION.marketid%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='DISTRIBUTION';
    col_name:='distid';
    checker:=0;
    CHECK_IF_EXISTS(id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            DISTRIBUTION
        SET
            DISTID=id,
            DISTNAME=name,
            DISTTYPEID=typeId,
            RELEASEDATE=release,
            RECLABELID=record,
            MARKETID=market
        WHERE
            DISTID=id;
        results:='1';
    else
        results:='0';
    end if;
end update_distribution;

    procedure update_contributor(
    id IN CONTRIBUTOR.contid%type,
    fname IN CONTRIBUTOR.firstname%type,
    lname IN CONTRIBUTOR.lastname%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='CONTRIBUTOR';
    col_name:='contid';
    checker:=0;
    CHECK_IF_EXISTS(id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            CONTRIBUTOR
        SET
            CONTID=id,
            FIRSTNAME=fname,
            LASTNAME=lname
        WHERE
            CONTID=id;
        results:='1';
    else
    results:='0';
    end if;
end update_contributor;

    procedure update_distribution_performance(
    dist_id IN DISTRIBUTION_PERFORMANCE.distid%type,
    perform_id IN DISTRIBUTION_PERFORMANCE.performid%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='DISTRIBUTION_PERFORMANCE';
    col_name:='distid';
    checker:=0;
    CHECK_IF_EXISTS(dist_id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            DISTRIBUTION_PERFORMANCE
        SET
            DISTID=dist_id,
            PERFORMID=perform_id
        WHERE
            DISTID=dist_id;
        results:='1';
    else
        results:='0';
    end if;
end update_distribution_performance;

    procedure update_distributiontypes(
    dist_type_id IN DISTRIBUTIONTYPES.disttypeid%type,
    type_name IN DISTRIBUTIONTYPES.typename%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='DISTRIBUTIONTYPES';
    col_name:='disttypeid';
    checker:=0;
    CHECK_IF_EXISTS(dist_type_id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            DISTRIBUTIONTYPES
        SET
            DISTTYPEID=dist_type_id,
            TYPENAME=type_name
        WHERE
            DISTTYPEID=dist_type_id;
        results:='1';
    else
        results:='0';
    end if;
end update_distributiontypes;

    procedure update_market(
    id IN MARKET.marketid%type,
    name IN MARKET.marketname%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='MARKET';
    col_name:='marketid';
    checker:=0;
    CHECK_IF_EXISTS(id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            MARKET
        SET
            MARKETID=id,
            MARKETNAME=name
        WHERE
            MARKETID=id;
        results:='1';
    else
        results:='0';
    end if;
end update_market;

    procedure update_performance(
    id IN PERFORMANCE.performid%type,
    name IN PERFORMANCE.performname%type,
    create_date IN PERFORMANCE.createdate%type,
    dur IN PERFORMANCE.duration%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='PERFORMANCE';
    col_name:='performid';
    checker:=0;
    CHECK_IF_EXISTS(id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            PERFORMANCE
        SET
            PERFORMID=id,
            PERFORMNAME=name,
            CREATEDATE=create_date,
            DURATION=dur
        WHERE
            PERFORMID=id;
        results:='1';
    else
        results:='0';
    end if;
end update_performance;

    procedure update_performance_component(
    compilation_id IN PERFORMANCE_COMPONENt.compilationid%type,
    component_id IN PERFORMANCE_COMPONENT.componentid%type,
    offset_from_start IN PERFORMANCE_COMPONENT.offsetfrombeginning%type,
    offset_from_self IN PERFORMANCE_COMPONENT.offsetfromself%type,
    duration_used IN PERFORMANCE_COMPONENT.durationused%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='PERFORMANCE_COMPONENT';
    col_name:='compilationid';
    checker:=0;
    CHECK_IF_EXISTS(component_id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            PERFORMANCE_COMPONENT
        SET
            COMPILATIONID=compilation_id,
            COMPONENTID=component_id,
            OFFSETFROMBEGINNING=offset_from_start,
            OFFSETFROMSELF=offset_from_self,
            DURATIONUSED=duration_used
        WHERE
            COMPILATIONID=compilation_id;
        results:='1';
    else
        results:='0';
    end if;
end update_performance_component;

    procedure update_PERFORMANCE_CONTRIBUTOR_ROLE(
    perform_id IN PERFORMANCE_CONTRIBUTOR_ROLE.performid%type,
    cont_id IN PERFORMANCE_CONTRIBUTOR_ROLE.contid%type,
    role_id IN PERFORMANCE_CONTRIBUTOR_ROLE.roleid%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='PERFORMANCE_CONTRIBUTOR_ROLE';
    col_name:='performid';
    checker:=0;
    CHECK_IF_EXISTS(perform_id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            PERFORMANCE_CONTRIBUTOR_ROLE
        SET
            PERFORMID=perform_id,
            CONTID=cont_id,
            ROLEID=role_id
        WHERE
            PERFORMID=perform_id;
        results:='1';
    else
        results:='0';
    end if;
end update_PERFORMANCE_CONTRIBUTOR_ROLE;

    procedure update_recordlabel(
    id IN RECORDLABEL.reclabelid%type,
    name IN RECORDLABEL.reclabelname%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='RECORDLABEL';
    col_name:='reclabelid';
    checker:=0;
    CHECK_IF_EXISTS(id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            RECORDLABEL
        SET
            RECLABELID=id,
            RECLABELNAME=name
        WHERE
            RECLABELID=id;
        results:='1';
    else
        results:='0';
    end if;
end update_recordlabel;

    procedure update_role(
    id in ROLE.ROLEID%TYPE,
    name IN role.rolename%type,
    results OUT varchar2
)
AS
    tableName varchar2(50);
    col_name varchar2(50);
    checker number;
BEGIN
    tableName:='ROLE';
    col_name:='roleid';
    checker:=0;
    CHECK_IF_EXISTS(id,tableName,col_name,checker);
    if (checker=1)then
        UPDATE
            ROLE
        SET
            ROLEID=id,
            ROLENAME=name
        WHERE
            ROLEID=id;
        results:='1';
    else
        results:='0';
    end if;
end update_role;


END all_updates;


