CREATE or REPLACE PACKAGE AddProPkg is
    v_isExist number(4, 2);
    PROCEDURE addComponent(
        p_compilationId PERFORMANCE_COMPONENT.COMPILATIONID%type,
        p_componentId PERFORMANCE_COMPONENT.COMPONENTID%type,
        p_offSetFromSelf PERFORMANCE_COMPONENT.OFFSETFROMSELF%type,
        p_offSetFromBegin PERFORMANCE_COMPONENT.OFFSETFROMBEGINNING%type,
        p_durationUsed PERFORMANCE_COMPONENT.DURATIONUSED%type,
        p_doesInserted out number);
    PROCEDURE addSinglePerformance(
        p_performName PERFORMANCE.performname%type,
        p_createDate PERFORMANCE.createDate%type,
        p_duration Performance.duration%type,
        p_doesInserted out number);
    procedure addDistribution(
        p_distName in DISTRIBUTION.distname%type,
        p_distTypeId in DIstribution.disttypeid%type,
        p_releaseDate in DISTRIBUTION.releaseDate%type,
        p_recLabelId in DISTRIBUTION.reclabelid%type,
        p_marketId in DISTRIBUTION.MARKETID%type,
        p_doesInserted out number);
    PROCEDURE addContributor(p_firstName in CONTRIBUTOR.firstname%type, p_lastName in Contributor.lastName%type, p_doesInserted out number);
    PROCEDURE addRecordLabel(p_recordName in RECORDLABEL.RECLABELNAME%type, p_doesInserted out number);
    PROCEDURE addMarket(p_marketName in MARKET.MARKETNAME%type, p_doesInserted out number);
    PROCEDURE addRole(p_roleName in ROLE.ROLENAME%type, p_doesInserted out number);
    PROCEDURE addDistributionType(p_distTypeName in DistributionTypes.typeName%type, p_doesInserted out number);
    FUNCTION doesContributorExist(p_firstName in CONTRIBUTOR.firstname%type, p_lastName in Contributor.lastName%type) RETURN NUMBER;
    FUNCTION doesDistributionExist(p_distName in DISTRIBUTION.distname%type) RETURN NUMBER;
    FUNCTION doesMarketExist(p_market in MARKET.MARKETNAME%type) RETURN NUMBER;
    FUNCTION doesRoleExist(p_roleName in ROLE.ROLENAME%type) RETURN NUMBER;
    FUNCTION doesRecordExist(p_recordName in RECORDLABEL.RECLABELNAME%type) RETURN NUMBER;
    FUNCTION doesDistributionTypeExist(p_distTypedName in DistributionTypes.typeName%type) RETURN NUMBER;
    FUNCTION doesPerformanceIdExist(p_performanceId in PERFORMANCE.PERFORMID%type) return number;
    FUNCTION doesPerformanceExist(p_performanceName in PERFORMANCE.PERFORMNAME%type) RETURN NUMBER;
end AddProPkg;
/

Create or REPLACE PACKAGE BODY AddProPkg is
    -- We must provide id and its name to eliminate possible errors. Otherwise, it'll be difficult to
--manage entering data. Because it is a sequence, so user can easily make an error.

/**
*@author
@param
*/
    PROCEDURE addSinglePerformance(
        p_performName PERFORMANCE.performname%type,
        p_createDate PERFORMANCE.createDate%type,
        p_duration Performance.duration%type,
        p_doesInserted out number)
        is
        v_isExist number;
    begin
        v_isExist := doesPerformanceExist(p_performName);

        if v_isExist = 0 then
            insert into PERFORMANCE
            values (PERFORMID_SEQUENCE.nextval,
                    p_performName,
                    p_createDate,
                    p_duration);
            p_doesInserted := 1;
        else
            p_doesInserted := 0;
        end if;

    end addSinglePerformance;

    PROCEDURE addComponent(
        p_compilationId PERFORMANCE_COMPONENT.COMPILATIONID%type,
        p_componentId PERFORMANCE_COMPONENT.COMPONENTID%type,
        p_offSetFromSelf PERFORMANCE_COMPONENT.OFFSETFROMSELF%type,
        p_offSetFromBegin PERFORMANCE_COMPONENT.OFFSETFROMBEGINNING%type,
        p_durationUsed PERFORMANCE_COMPONENT.DURATIONUSED%type,
        p_doesInserted out number
    ) is
        v_isExist number;
    begin
        --Same component or compilation can be used for different compilation
        v_isExist := doesPerformanceIdExist(p_compilationId);
        if v_isExist = 1 then
            insert into PERFORMANCE_COMPONENT
            values (p_compilationId, p_componentId, p_offSetFromBegin, p_offSetFromSelf, p_durationUsed);

            p_doesInserted := 1;
        else
            p_doesInserted := 0;
        end if;
    end addComponent;


    procedure addDistribution(
        p_distName in DISTRIBUTION.distname%type,
        p_distTypeId in DIstribution.disttypeid%type,
        p_releaseDate in DISTRIBUTION.releaseDate%type,
        p_recLabelId in DISTRIBUTION.reclabelid%type,
        p_marketId in DISTRIBUTION.MARKETID%type,
        p_doesInserted out number
    ) is
        v_isExist number(4, 2);
    begin
        v_isExist := doesDistributionExist(p_distName);

        if v_isExist = 0 then
            insert into Distribution
            values (distId_sequence.nextval,
                    p_distName,
                    p_distTypeId,
                    p_releaseDate,
                    p_recLabelId,
                    p_marketId);
            p_doesInserted := 1;
        else
            p_doesInserted := 0;
        end if;
    end addDistribution;
    PROCEDURE addDistributionType(
        p_distTypeName in DistributionTypes.typeName%type,
        p_doesInserted out number
    ) is
        v_isExist number(4, 2);
    begin
        v_isExist := doesDistributionTypeExist(p_distTypeName);

        if v_isExist = 0 then
            insert into DistributionTypes
            values (distTypeId_sequence.nextval, p_distTypeName);
            p_doesInserted := 1;
        else
            p_doesInserted := 0;
        end if;

    end addDistributionType;
/**
 It adds a new contributor into DB. Id displays a number with success 1 and 0 is not success.
 */
    PROCEDURE addContributor(
        p_firstName in CONTRIBUTOR.firstname%type,
        p_lastName in Contributor.lastName%type,
        p_doesInserted out number
    ) is
        v_isExist number(4, 2);
    begin
        v_isExist := doesContributorExist(p_firstName, p_lastName);

        if v_isExist = 0 then
            insert into Contributor
            values (contId_sequence.nextval, p_firstName, p_lastName);
            p_doesInserted := 1;
        else
            p_doesInserted := 0;
        end if;
    end addContributor;
/**
 It adds a new record label into DB. It displays a number with success 1 and 0 is not success.
 */
    PROCEDURE addRecordLabel(
        p_recordName in RECORDLABEL.RECLABELNAME%type,
        p_doesInserted out number
    ) is
        v_isExist number(4, 2);
    begin
        v_isExist := doesRecordExist(p_recordName);

        if v_isExist = 0 then
            insert into RecordLabel
            values (reclabelId_sequence.nextval, p_recordName);
            p_doesInserted := 1;
        else
            p_doesInserted := 0;
        end if;

    end addRecordLabel;
/**
 It adds a new Market into DB with a given market's name. It displays a number with success 1, 0 is not success.
 */
    PROCEDURE addMarket(
        p_marketName in MARKET.MARKETNAME%type,
        p_doesInserted out number
    ) is
        v_isExist number(4, 2);
    begin
        v_isExist := doesMarketExist(p_marketName);

        if v_isExist = 0 then
            insert into Market
            values (marketId_sequence.nextval, p_marketName);
            p_doesInserted := 1;
        else
            p_doesInserted := 0;
        end if;

    end addMarket;
/**
 This procedure add a new Role if role does not exist, role exist function checks the role is exist then adds it
 @param p_roleName name of the role
 @param p_roleUsage out parameter of function to capture in java
 */
    PROCEDURE addRole(
        p_roleName in ROLE.ROLENAME%type,
        p_doesInserted out number
    ) is
        v_isExist number(4, 2);
    begin
        v_isExist := doesRoleExist(p_roleName);

        if v_isExist = 0 then
            insert into Role
            values (roleId_sequence.nextval, p_roleName);
            p_doesInserted := 1;
        else
            p_doesInserted := 0;
        end if;
    end addRole;
---FUNCTIONS
/**
 It checks, is there the contributor in the DB. It returns with number 0 mean that id does not exist, 1 exist.
 */
    FUNCTION doesDistributionExist(p_distName in DISTRIBUTION.distname%type)
    return number is
        v_isExist    number(4, 2);
        v_totalCount number(4, 2);
    BEGIN

        Select count(*)
        into v_totalCount
        from DISTRIBUTION
        where DISTNAME = p_distName;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    END doesDistributionExist;
/**
 It checks, is there the contributor in the DB. It returns with number 0 mean that id does not exist, 1 exist.
 */
    FUNCTION doesContributorExist(
        p_firstName in CONTRIBUTOR.firstname%type,
        p_lastName in Contributor.lastName %type
    ) return number is
        v_isExist    number(4, 2);
        v_totalCount NUMBER(4, 2);
    BEGIN

        SELECT count(*)
        into v_totalCount
        FROM CONTRIBUTOR
        WHERE FIRSTNAME = p_firstName
          and LASTNAME = p_lastName;
        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    END doesContributorExist;

/**
 It checks, is there a specific market in the DB. Returns with number 0 mean that id does not exist, 1 exist.
 */
    FUNCTION doesMarketExist(p_market in MARKET.MARKETNAME%type) return number is
        v_isExist    number(4, 2);
        v_totalCount number(4, 2);
    BEGIN

        SELECT count(*)
        into v_totalCount
        FROM MARKET
        WHERE MARKETNAME = p_market;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    END doesMarketExist;
/**
 It checks, is there a specific role in the DB. Returns with number 0 mean that id does not exist, 1 exist.
 */
    FUNCTION doesRoleExist(p_roleName in ROLE.ROLENAME%type) return number as
        v_isExist    number(4, 2);
        v_totalCount number(4, 2);
    begin

        SELECT count(*)
        into v_totalCount
        FROM ROLE
        where ROLENAME = p_roleName;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesRoleExist;
/**
 It checks, is there a specific record label in the DB. Returns with number 0 mean that id does not exist, 1 exist.
 */
    FUNCTION doesRecordExist(p_recordName in RECORDLABEL.RECLABELNAME%type) return number is
        v_isExist    number(4, 2);
        v_totalCount number(4, 2);
    begin

        SELECT count(*)
        into v_totalCount
        FROM RECORDLABEL
        where RECLABELNAME = p_recordName;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;

    end doesRecordExist;

    FUNCTION doesDistributionTypeExist(p_distTypedName in DistributionTypes.typeName%type) return number
    is
        v_isExist    number(4, 2);
        v_totalCount number(4, 2);
    begin

        SELECT count(*)
        into v_totalCount
        FROM DISTRIBUTIONTYPES
        where TYPENAME = p_distTypedName;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesDistributionTypeExist;

    FUNCTION doesPerformanceExist(p_performanceName in PERFORMANCE.PERFORMNAME%type) return number
    is
        v_isExist    number(4, 2);
        v_totalCount number(4, 2);
    begin

        SELECT count(*)
        into v_totalCount
        FROM PERFORMANCE
        where PERFORMNAME = p_performanceName;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesPerformanceExist;

    FUNCTION doesPerformanceIdExist(
        p_performanceId in PERFORMANCE.PERFORMID%type
    ) return number is
        v_isExist    number(4, 2);
        v_totalCount number(4, 2);
    begin

        SELECT count(*)
        into v_totalCount
        FROM PERFORMANCE
        where PERFORMID = p_performanceId;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesPerformanceIdExist;
end AddProPkg;
