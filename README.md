1. To install: in the sql folder run BUILD.sql, then run GET_DATA_PACKAGE.sql, run AddProPkg.sql, and run delProPkg.sql
2. To run: in the src folder run the main method in Application.java
3. To uninstall: in the sql folder run UNINSTALL.sql
When running the unit tests, make sure the data has not changed: you can run BUILD.sql again to make sure.
To run the unit tests you need to create your own Credentials.java like so:

public class Credentials{
    /**
     * 
     * @return The username used with PDBORA19C
     */
    public String getDBUser(){
        return "yourUsername";
    }
    /**
     * 
     * @return The password used with PDBORA19C
     */
    public String getDBPwd(){
        return "yourPassword";
    }
}
