

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a contributor and roles 
 * (if it is parented by a performance object the roles are specific to that performance, 
 * otherwise the roles are all of the contributors general roles)
 * @author Adam Atamnia
 */
public class Contributor {
    private int contId;
    private String contName;
    private List<String> roles;

    public Contributor(int contId, String contName, List<String> roles) {

        if(contName == null || roles == null){
            throw new IllegalArgumentException();
        }
        this.contId = contId;
        this.contName = contName;
        this.roles = new ArrayList<String>(roles);
    }
    
    public int getContId() {
        return contId;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        Contributor contributor = (Contributor)obj;
        if (
            this.contId == contributor.contId &&
            this.contName.equals(contributor.contName) &&
            this.roles.equals(contributor.roles)
            ) {
            isEqual = true;
        }
        return isEqual;
    }

    /**
     * Returns a formated string with only the roles of the contributor, used in showOnlyOneContributor(int contId)
     * @return
     */
    public String showRolesOnly() {       
        StringBuilder builder = new StringBuilder();
        builder.append("Role(s): ");
        for (String string : roles) {
            builder.append(string+", ");
        } 

        return builder.toString();
    }

    @Override
    public String toString() {       
        StringBuilder builder = new StringBuilder("| "+contId+" | "+contName+" | ");
        builder.append("Role(s): ");
        for (String string : roles) {
            builder.append(string+", ");
        } 

        return builder.toString();
    }
    
}
