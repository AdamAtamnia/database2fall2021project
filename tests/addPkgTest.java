package tests;


import java.sql.Date;
import java.sql.SQLException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import src.BusinessLogic;
import src.Credentials;

public class addPkgTest {

    @Test
    public void insertRoleTest() throws SQLException {
        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        
        try {
            Assertions.assertEquals(1, bl.addRole("java Tester"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void insertMarketTest() throws SQLException {
        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
      
        try {
            Assertions.assertEquals(1, bl.addMarket("dorval"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

     @Test
    public void insertRecordLabelTest() throws SQLException {
        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        try {
            Assertions.assertEquals(1, bl.addRecordLabel("Lightning CORPS."));
        } catch (SQLException e) {
            e.printStackTrace();
        }
   
    }

    @Test
    public void insertContributorTest() throws SQLException {

        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        try {
            Assertions.assertEquals(1, bl.addContributor("Kermit", "JR"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    
    @Test
    public void insertDistributionTypeTest() throws SQLException {

        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        try {
            Assertions.assertEquals(1, bl.addDistributionType("Soundtrack"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void insertDistributionTest() throws SQLException {
        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        try {
            Assertions.assertEquals(1, bl.addDistribution("DawsonDist", 1, Date.valueOf("2020-03-01"), 1, 1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void insertPerformanceTest() throws SQLException {
        Credentials credentials = new Credentials();
        BusinessLogic bl = new BusinessLogic(credentials);
        
        try {
            Assertions.assertEquals(1, bl.addSinglePerformance("Test",  Date.valueOf("2021-11-01"), "00:34:00"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }



}
