import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import java.io.Console;

public class Application {
    public static void main(String[] args) {
        var console = System.console();
        BusinessLogic businessLogic = null;

        System.out.println("Greetings!");
        System.out.println("Choose one of the following options:\n" +
                "1. Login\n" +
                "2. Quit");

        switch (getDecision(2, console)) {
            case 1:
                businessLogic = login(console);
                mainMenuPrompt(businessLogic ,console);
                break;
            case 2:
                System.out.println("Bye Bye!");
                break;
            default:
                throw new RuntimeException("this should never happen");
        }

    }
 
    /**
     * Prompts the user to decide an option in the main menu
     * @param bl
     * @param console
     * @author Adam Atamnia
     */
    private static void mainMenuPrompt(BusinessLogic bl,Console console) {
        try {
            boolean wantToQuit = false;
            while (!wantToQuit) {
                printMainMenuChoices();
                switch (getDecision(9, console)) {
                    case 1:
                        showPerformance(bl, console, false);
                        break;
                    case 2:
                        showPerformance(bl, console, true);
                        break;
                    case 3:
                        getAlterChoice(bl, console);
                        // insertDelete(bl, console);
                        break;
                    case 4:
                        showDistribution(bl, console, false);
                        break;
                    case 5:
                        showDistribution(bl, console, true);
                        break;
                    case 6:
                        showActivityLogs(bl, console);
                        break;
                    case 7:
                        showContributorRoles(bl, console, true);
                        break;
                    case 8:
                        showContributorRoles(bl, console, false);
                        break;
                    case 9:
                        wantToQuit = true;
                        System.out.println("Bye Bye!");
                        break;
                    default:
                        throw new RuntimeException("this should never happen");
                }
            }
        } catch (SQLException e) {
            System.out.println("An error occured, stopping the application");
        }
        
    }
    private static void getAlterChoice(BusinessLogic bl, Console console){
        // try {
            boolean wantToQuit = false;
            while (!wantToQuit) {
                printAlterDBChoices();
                switch (getDecision(4, console)) {
                    case 1:
                        insertDatabase(bl,console);
                        break;
                    case 2:
                        uiUpdate(bl, console);
                        break;
                    case 3:
                        deleteDatabase(bl,console);
                        break;                    
                    case 4:
                        wantToQuit = true;
                        System.out.println("Bye Bye!");
                        break;
                    default:
                        throw new RuntimeException("this should never happen");
                }
            }
        // } catch (SQLException e) {
        //     System.out.println("An error occured, stopping the application");
        // }
    }
    private static void uiUpdate(BusinessLogic bl, Console console){
        Scanner scan=new Scanner(System.in);
        try {
            boolean wantToQuit = false;
            while (!wantToQuit) {
                printAllUpdateChoices();
                switch (getDecision(11, console)) {
                    case 1://CONTRIBUTOR
                        callUpdateContributor(bl, scan);
                        break;
                    case 2://DISTRIBUTION
                        callUpateDistribution(bl, scan);
                        break;
                    case 3://DISTRIBUTION_PERFORMANCE
                        callUpdateDistributionPerformance(bl, scan);
                        break;
                    case 4://DISTRIBUTIONTYPES
                        callUpdateDistributionTypes(bl, scan);
                        break;
                    case 5://MARKET
                        callUpdateMarket(bl, scan);
                        break;
                    case 6://PERFORMANCE
                        callUpdatePerformance(bl, scan);
                        break;
                    case 7://PERFORMANCE_COMPONENT
                        callUpdatePerformanceComponent(bl, scan);
                        break;
                    case 8://PERFORMANCE_CONTRIBUTOR_ROLE
                        callUpdatePerformanceContributorRole(bl, scan);
                        break;
                    case 9://RECORDLABEL
                        callUpdateRecordLabel(bl, scan);
                        break;
                    case 10://ROLE
                        callUpdateRole(bl, scan);
                        break;
                    case 11://QUIT
                        wantToQuit = true;
                        System.out.println("Bye Bye!");
                        break;
                    default:
                        throw new RuntimeException("this should never happen");
                }
            }
        } catch (SQLException e) {
            System.out.println("An error occured, stopping the application");
        }
    }

    private static void printAllUpdateChoices(){
        System.out.println("You are now in the main menu");
        System.out.println("----------------------------");
        System.out.println("Choose one of the following options:\n" +
                "1. CONTRIBUTOR \n" +
                "2. DISTRIBUTION \n" +
                "3. DISTRIBUTION_PERFORMANCE \n" + 
                "4. DISTRIBUTIONTYPES \n" +
                "5. MARKET \n" +
                "6. PERFORMANCE \n" +
                "7. PERFORMANCE_COMPONENT \n" +
                "8. PERFORMANCE_CONTRIBUTOR_ROLE \n" +
                "9. RECORDLABEL \n"+
                "10. ROLE \n"+
                "11. BACK");
    }

    private static void printAlterDBChoices(){
        System.out.println("Select in which way you want to alter the database");
        System.out.println("----------------------------");
        System.out.println("Choose one of the following options:\n" +
                "1. Insert\n" +
                "2. Update\n" +
                "3. Delete\n" + 
                "4. BACK");
    }

    /**
     * Prints main menu choices
     * @author Adam Atamnia
     */
    private static void printMainMenuChoices() {
        System.out.println("You are now in the main menu");
        System.out.println("----------------------------");
        System.out.println("Choose one of the following options:\n" +
                "1. For a given performance: show all contributers and their roles\n" +
                "2. For a given performance: get all Performance details (recursively)\n" +
                "3. Change data in the database (insert, add or delete)\n" + 
                "4. For a given distribution: get all performances\n" +
                "5. For a given distribution: get all performances with all details (recursively)\n" +
                "6. For a given time period: get all activity logs\n" +
                "7. For a given contributor: show all performances they played with their roles\n" +
                "8. For a given contributor: show all roles irrespective of performance\n" +
                "9. Quit");
    }

    /**
     * Shows a given performance, if isRecursive: show all details recursively, else will only show contributors and roles
     * @param bl
     * @param console
     * @throws SQLException
     * @author Adam Atamnia
     */
    private static void showPerformance(BusinessLogic bl, Console console, boolean isRecursive) throws SQLException {
        System.out.println(bl.getAllPerformances());
        System.out.println("Above are all the perfromances in the database: choose one");
        boolean isChoiceValid = false;
        do {
            try {
                APerformance performance = bl.getPerformance(getId(console));
                if(isRecursive){
                    System.out.println(performance.toString());
                } else {
                    System.out.println(performance.showContributorsAndRoles());
                }
                isChoiceValid = true;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                System.out.println("Try Again");
            }
        } while (!isChoiceValid);
    }

    /**
     * Shows a given distribution, if isRecursive: show all details recursively, 
     * else will only show all performance info except contributors and components
     * @param bl
     * @param console
     * @throws SQLException
     * @author Adam Atamnia
     */
    private static void showDistribution(BusinessLogic bl, Console console, boolean isRecursive) throws SQLException {
        System.out.println(bl.getAllDistributions());
        System.out.println("Above are all the distributions in the database: choose one");
        boolean isChoiceValid = false;
        do {
            try {
                Distribution distribution = bl.getDistribution(getId(console));
                if(isRecursive){
                    System.out.println(distribution.toString());
                } else {
                    System.out.println(distribution.showAllPerformancesWithoutContributorsAndRoles());
                }
                isChoiceValid = true;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                System.out.println("Try Again");
            }
        } while (!isChoiceValid);
    }

    /**
     * Shows a given distribution, if isRecursive: show all details recursively, 
     * else will only show all performance info except contributors and components
     * @param bl
     * @param console
     * @throws SQLException
     * @author Adam Atamnia
     */
    private static void showContributorRoles(BusinessLogic bl, Console console, boolean isFiltered) throws SQLException {
        System.out.println(bl.getAllContributorsFromTable());
        System.out.println("Above are all the contributors in the database: choose one");
        boolean isChoiceValid = false;
        do {
            try {
                if(isFiltered){
                    int contId = getId(console);
                    List<APerformance> performances = selectFilters(bl, console, contId);
                    System.out.println("Performance(s):");
                    for (APerformance performance : performances) {
                        System.out.println(performance.showOnlyOneContributor(contId));
                    }
                } else {
                    List<String> strings = bl.getAllRoles(getId(console));
                    System.out.println("Role(s):");
                    for (String string : strings) {
                        System.out.println("- "+string);
                    }
                }
                isChoiceValid = true;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                System.out.println("Try Again");
            }
        } while (!isChoiceValid);
    }
    
    /**
     * Asks the user to select the filters for getPerformancesFromContributor(...) and calls it
     * @param bl
     * @param console
     * @param contId
     * @return List<APerformance>
     * @throws SQLException
     * @author Adam Atamnia
     */
    private static List<APerformance> selectFilters(BusinessLogic bl, Console console,int contId) throws SQLException {
        boolean releasedOnly = false;
        boolean notReleasedOnly = false;
        boolean compilationOnly = false;
        boolean recordingOnly = false;

        System.out.println("Do you want the performances to be filtered");
        if (getYesNo(console)) {
            System.out.println("Do you want the performances to be Compilations only");
            if (getYesNo(console)) {
                compilationOnly = true;
            } else {
                System.out.println("Do you want the performances to be Recordings only");
                if (getYesNo(console)) {
                    recordingOnly = true;
                }
            }

            System.out.println("Do you want the performances to be Released only");
            if (getYesNo(console)) {
                releasedOnly = true;
            } else {
                System.out.println("Do you want the performances to be Not Released only");
                if (getYesNo(console)) {
                    notReleasedOnly = true;
                }
            }
        }
        return bl.getPerformancesFromContributor(contId, releasedOnly, notReleasedOnly, compilationOnly, recordingOnly);
    }

    /**
     * Given a time period show all activity logs
     * @param bl
     * @param console
     * @throws SQLException
     * @author Adam Atamnia
     */
    private static void showActivityLogs(BusinessLogic bl, Console console) throws SQLException {
        boolean isChoiceValid = false;
        do {
            System.out.println("All entered dates have to be in this format (everything in [] is optional): yyyy-[m]m-[d]d");
            String startDate = console.readLine("(Enter an appropriate start date): ");
            String endDate = console.readLine("(Enter an appropriate end date): ");
            try {
                List<ActivityLog> activityLogs = bl.getActivityLogs(Date.valueOf(startDate), Date.valueOf(endDate));
                System.out.println("Activity Logs:");
                for (ActivityLog activityLog : activityLogs) {
                    System.out.println(activityLog.toString());
                }
                isChoiceValid = true;
            } catch (IllegalArgumentException e) {
                if(e.getMessage() == null){
                    System.out.println("Date in wrong format");
                } else {
                    System.out.println(e.getMessage());
                }
                System.out.println("Try Again");
            }
        } while (!isChoiceValid);
    }

    /**
     * logs in using the Credentials.java class (FOR OUR TESTING ONLY NOT USED IN FINAL BUID: NOT USER FRIENDLY)
     * @param console
     * @return connected businessLogic
     * @author Adam Atamnia
     */                                                                                                                                                                                                                                     
    // private static BusinessLogic loginWithCredentialObj(Console console) {
    //     Credentials credentials = new Credentials();
    //     BusinessLogic businessLogic = null;
    //     try {
    //         businessLogic = new BusinessLogic(credentials);
    //     } catch (SQLException e) {
    //         System.out.println("Make sure your creadentials class contains valid username and password and check your connection");
    //         System.out.println("Try Again");
    //     }
    //     return businessLogic;
    // }

    /**
     * asks the user to login and to relogin if login info was invalid
     * @param console
     * @return connected businessLogic
     * @author Adam Atamnia
     */
    private static BusinessLogic login(Console console) {
        boolean isLoginValid = false;
        BusinessLogic businessLogic = null;
        do {
            try {
                String userName = console.readLine("Enter your username: ");
                String pwd = new String(console.readPassword("Enter your password: "));

                businessLogic = new BusinessLogic(userName, pwd);
                isLoginValid = true;
            } catch (SQLException e) {
                System.out.println("Please enter a valid username and password and check your connection");
                System.out.println("Try Again");
            }
        } while (!isLoginValid);
        return businessLogic;
    }

    /**
     * asks the user to input a number an makes sure its an integer and in the given range
     * @param console
     * @return valid number
     * @author Adam Atamnia
     */
    public static int getDecision(int range, Console console) {
        boolean isChoiceValid = false;
        int input = 0;
        do {
            try {
                String choice = console.readLine("(Enter an appropriate number): ");
                input = Integer.parseInt(choice);

                if(input < 1 || input > range){
                    System.out.println("Please enter a number in the range 1-"+range);
                    System.out.println("Try Again");
                }
                else{
                    isChoiceValid = true;
                }
            } catch (NumberFormatException e) {
                System.out.println("Please enter an integer");
                System.out.println("Try Again");
            }
        } while (!isChoiceValid);
        return input;
    }

    /**
     * asks the user to input a number an makes sure its an integer
     * @param console
     * @return int
     * @author Adam Atamnia
     */
    public static int getId(Console console) {
        boolean isChoiceValid = false;
        int input = 0;
        do {
            try {
                String choice = console.readLine("(Enter an appropriate id): ");
                input = Integer.parseInt(choice);
                isChoiceValid = true;
            } catch (NumberFormatException e) {
                System.out.println("Please enter an integer");
                System.out.println("Try Again");
            }
        } while (!isChoiceValid);
        return input;
    }

    /**
     * asks the user to input y or n (yes or no), asks again if they entered something else
     * @param console
     * @return boolean true or false
     * @author Adam Atamnia
     */
    public static boolean getYesNo(Console console) {
        boolean isChoiceValid = false;
        String choice = null;
        do {
            choice = console.readLine("(Enter y/n): ").toLowerCase();
            if(choice.equals("y") || choice.equals("n")){
                isChoiceValid = true;
            }else{
                System.out.println("Please enter 'y' or 'n'");
                System.out.println("Try Again");
            }
            
        } while (!isChoiceValid);
        return choice.equals("y");
    }

    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpdateContributor(BusinessLogic bl,Scanner scan) throws SQLException{
        System.out.println("Enter the Id of the contributor you want to update");
        int id=scan.nextInt();
        System.out.println("Enter the new first name for the contributor");
        String newFName=scan.next();
        System.out.println("Enter the new last name for the contributor");
        String newLName=scan.next();
        String result;
        result=bl.updateContributor(id,newFName,newLName);
        System.out.println(result);
    }
    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpateDistribution(BusinessLogic bl,Scanner scan) throws SQLException{
        System.out.println("Enter the id of the distribution you want to update");
        int distId=scan.nextInt();
        System.out.println("Enter the new distribution name");
        String newDistName=scan.next();
        System.out.println("Enter the new distribution type id");
        int newTypeId=scan.nextInt();
        System.out.println("Enter the new release date in format yyyy-mm-dd");
        String stringDate=scan.next();
        Date newReleaseDate=Date.valueOf(stringDate);
        System.out.println("Enter the new record label id");
        int newRecLabelId=scan.nextInt();
        System.out.println("Enter the new market id");
        int newMarketId=scan.nextInt();
        String result=bl.updateDistribution(distId, newDistName, newTypeId, newReleaseDate, newRecLabelId, newMarketId);
        System.out.println(result);
    }
    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpdateDistributionPerformance(BusinessLogic bl,Scanner scan) throws SQLException{
        System.out.println("Enter the id for the entry of theDistributionPerformance table");
        int id=scan.nextInt();
        System.out.println("Enter the new performance id");
        int newPerformId=scan.nextInt();
        String result=bl.updateDistributionPerformance(id, newPerformId);
        System.out.println(result);
    }
    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpdateDistributionTypes(BusinessLogic bl,Scanner scan) throws SQLException{
        System.out.println("Enter the distribution id from the Distributiontypes table that you want to update");
        int id=scan.nextInt();
        System.out.println("Enter the new distrinution type");
        String newTypeName=scan.next();        
        String result=bl.updateDistributionTypes(id, newTypeName);
        System.out.println(result);
    }
    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpdateMarket(BusinessLogic bl,Scanner scan) throws SQLException{
        System.out.println("Enter the market id that you want to update");
        int id=scan.nextInt();
        System.out.println("Enter the new market name");
        String newMarketName=scan.next();
        String result=bl.updateMarket(id, newMarketName);
        System.out.println(result);
    }
    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpdatePerformance(BusinessLogic bl, Scanner scan) throws SQLException{
        System.out.println("Enter the performance id you wan to update");
        int id=scan.nextInt();
        System.out.println("Enter the new performance name");
        String newPerformanceName=scan.next();
        System.out.println("Enter the new creating date in format YYYY-MM-DD");
        String stringDate=scan.next();
        Date newCreateDate=Date.valueOf(stringDate);
        System.out.println("Enter the new duration");
        String newDuration=scan.next();
        String result=bl.updatePerformance(id, newPerformanceName, newCreateDate, newDuration);
        System.out.println(result);
    }
    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpdatePerformanceComponent(BusinessLogic bl,Scanner scan) throws SQLException{
        System.out.println("Enter performance id you want to update");
        int id=scan.nextInt();
        System.out.println("Enter the new component id");
        int newConponentId=scan.nextInt();
        System.out.println("Enter the new offset from beginnning");
        String newOffsetFromBeginning=scan.next();
        System.out.println("Enter the new offset from self");
        String newOffsetFromSelf=scan.next();
        System.out.println("Enter the new duration used");
        String newDurationUsed=scan.next();
        String result=bl.updatePerformanceComponent(id, newConponentId, newOffsetFromBeginning, newOffsetFromSelf, newDurationUsed);
        System.out.println(result);
    }
    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpdatePerformanceContributorRole(BusinessLogic bl,Scanner scan) throws SQLException{
        System.out.println("Enter the performance id on the entry from the PERFORMANCE_CONTRIBUTOR_ROLE table you wish to update");
        int id=scan.nextInt();
        System.out.println("Enter the new contributor id");
        int newContId=scan.nextInt();
        System.out.println("Entert the new role id");
        int newRoleId=scan.nextInt();
        String result=bl.updatePerformanceContributorRole(id, newContId, newRoleId);
        System.out.println(result);
    }
    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpdateRecordLabel(BusinessLogic bl,Scanner scan) throws SQLException{
        System.out.println("Enter the id of the record label you want to update");
        int id=scan.nextInt();
        System.out.println("Enter the new name of the record label");
        String newName=scan.next();
        String result=bl.updateRecordLabel(id, newName);
        System.out.println(result);
    }
    
    /** 
     * @param bl
     * @param scan
     * @throws SQLException
     */
    public static void callUpdateRole(BusinessLogic bl,Scanner scan) throws SQLException{
        System.out.println("Enter the id of the role you want to update");
        int id=scan.nextInt();
        System.out.println("Enter the new name of the role");
        String newName=scan.next();
        String result=bl.updateRecordLabel(id, newName);
        System.out.println(result);
    }



/**
 * @author Cuneyt YILDIRIM
 * @param bl
 */
    public static void insertDelete(BusinessLogic bl, Console console){

        System.out.println("What do you want to do?");
        System.out.println(
            "1. Insert Data\n"+
            "2. Update Database\n"+
            "3. Delete Database\n"+
                        "\nSelect a number above.");
        int menuDecision = getDecision(3, console);
        switch (menuDecision) {
            case 1:
                insertDatabase(bl, console);
                break;
            case 2:
                uiUpdate(bl, console);
                break;
            case 3:
                deleteDatabase(bl, console);
                break;
            default:
                System.out.println("Please select within the range.");
        }
    }


/**
 * @author Cuneyt YILDIRIM
 * @param bl
 */
    public static void insertDatabase(BusinessLogic bl, Console console)  {
        System.out.println("What do you want to insert\n"+
        "1. Insert a new SinglePerformance\n"+
        "2. Insert a new Distribution\n"+
        "3. Insert a new Record Label\n"+
        "4. Insert a new Market\n"+
        "5. Insert a new Distribution Type\n"+
        "6. Insert a new Role.\n"+
        "7. Insert a new Performance with Component\n"+
        "Hit the number above.");
    

        int insertDecision = getDecision(7, console);
        switch (insertDecision) {
            case 1:
                insertSinglePerformance(bl);
                break;
            case 2:
                insertDistribution(bl);
                break;
            case 3:
                insertRecordLabel( bl);
                break;
            case 4:
                insertMarket( bl);
                break;
            case 5:
                insertDistributionType( bl);
                break;
            case 6:
                insertRole(bl);
                break;
            case 7:
                insertPerformanceComponents(bl);
            default:
                System.out.println("Please select within the range.");
        }
    }

/**
 * @author Cuneyt YILDIRIM
 * @param bl
 */

    public static void deleteDatabase(BusinessLogic bl, Console console) {
        System.out.println("What do you want to insert\n" +
                "1. Delete a Performance\n" +
                "2. Delete a Distributor\n" +
                "3. Delete a Record Label\n" +
                "4. Delete a Market\n" +
                "5. Delete a Distribution Type\n" +
                "6. Delete a Role.\n" +
                "Hit the number above.");

        int insertDecision = getDecision(6, console);
        switch (insertDecision) {
            case 1:
                deletePerformance(bl);
                break;
            case 2:
                deleteDistribution(bl);
                break;
            case 3:
                deleteRecordLabel(bl);
                break;
            case 4:
                deleteMarket( bl);
                break;
            case 5:
                deleteDistributionType( bl);
                break;
            case 6:
                deleteRole(bl);
                break;
            default:
                System.out.println("Please select within the range.");
        }
    }



    /**
     * Gets to insert a Single Performance necessary information.
     * @author Cuneyt YILDIRIM
     */
    public static void insertSinglePerformance(BusinessLogic bl) {
        
        String performName = getInput("Enter to insert a new Performance Name");
        String rawDate = getInput("Enter to insert a new Creation Date the given format --> 'yyyy-mm-dd'");
        String duration = getInput("Enter to insert a new Duration Time the given format --> '00:00:00'");
        Date createDate = formatDate(rawDate);
       
        int retrn;
        try {
            retrn = bl.addSinglePerformance(performName, createDate, duration);
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }  
    }

        /**
     * Gets to insert a Single Performance necessary information.
     * @author Cuneyt YILDIRIM
     */
    public static void insertPerformanceComponents(BusinessLogic bl) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter to insert Component id");
        int componentId =   sc.nextInt();
        System.out.println("Enter to insert Compilation id");
        int compilationId = sc.nextInt();
        String offSetBegin = getInput("Enter to insert the off set time beginning the given format --> '00:00:00'");
        String offSetSelf = getInput("Enter to insert the off set time it self the given format --> '00:00:00'");
        String duration = getInput("Enter to insert the duration time the given format --> '00:00:00'");
       
        int retrn;
        try {
            retrn = bl.addPerformanceComponents(compilationId,componentId, offSetBegin, offSetSelf, duration);
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        } 
        sc.close(); 
    }
    /**
     * Gets to insert a Distribution necessary information.
     * @author Cuneyt YILDIRIM 
     */
    public static void insertDistribution(BusinessLogic bl) {
        String distname = getInput("Enter to insert the Distribution Name");
        String rawdistTypeId = getInput("Enter to insert a new Distribution Type id");
        String rawrecLabelId = getInput("Enter to insert a new Record Label id");
        String rawmarketId = getInput("Enter to insert a new Market id");
        String rawDate = getInput("Enter to insert a date given format -->'yyyy-mm-dd'");
        Date releaseDate = formatDate(rawDate);

        int retrn;
        try {
            retrn = bl.addDistribution(distname, Integer.parseInt(rawdistTypeId), releaseDate, Integer.parseInt(rawrecLabelId),Integer.parseInt(rawmarketId));
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }

    /**
     * Gets the necessary information to insert a Distribution Type.
     * @author Cuneyt YILDIRIM
     */
    public static void insertDistributionType(BusinessLogic bl) {
        String distType = getInput("Enter to insert a new Distribution Type Name");
        int retrn;
        try {
            retrn = bl.addDistributionType(distType);
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }

    /**
     * Gets the necessary information to insert a Record Label.
     * @author Cuneyt YILDIRIM
     */
    public static void insertRecordLabel(BusinessLogic bl) {
        String recLabel = getInput("Enter to insert a new Record Label Name");
        int retrn;
        try {
            retrn = bl.addRecordLabel(recLabel);;
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    /**
     * Gets the necessary information to insert a new Market Name.
     * @author Cuneyt YILDIRIM
     */
    public static void insertMarket(BusinessLogic bl) {
        String market = getInput("Enter to insert a new Market Name");
        int retrn;
        try {
            retrn = bl.addMarket(market);;
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    /**
     * Gets the necessary information to insert a new Role.
     * @author Cuneyt YILDIRIM
     * @throws SQLException
     */
    public static void insertRole(BusinessLogic bl) {
        String role = getInput("Enter Role");
        int retrn = 0;
        try {
            retrn = bl.addRole(role);
            System.out.println(retrn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * @description Gets the necessary information to delete Performance id.
     * @author Cuneyt YILDIRIM
     */
    public static void deletePerformance(BusinessLogic bl) {
        String performId = getInput("Enter to delete Performance id");
        int retrn;
        try {
            retrn = bl.delSinglePerformance(Integer.parseInt(performId));
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description Gets the necessary information to delete Distribution id.
     * @author Cuneyt YILDIRIM
     */
    public static void deleteDistribution(BusinessLogic bl) {
        String distributionId = getInput("Enter to delete Distribution id");
        int retrn;
        try {
            retrn = bl.delDistribution(Integer.parseInt(distributionId));
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description Gets the necessary information to delete Distribution Type name.
     * @author Cuneyt YILDIRIM
     */
    public static void deleteDistributionType(BusinessLogic bl) {
        String distTypeName = getInput("Enter to delete Distribution Type Name");
        int retrn;
        try {
            retrn = bl.delDistributionType(Integer.parseInt(distTypeName));
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description Gets the necessary information to delete Record Label id.
     * @author Cuneyt YILDIRIM
     */
    public static void deleteRecordLabel(BusinessLogic bl) {
        String recordLabelId = getInput("Enter to delete Record Label id");
        int retrn;
        try {
            retrn = bl.delRecordLabel(Integer.parseInt(recordLabelId));
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description Gets the necessary information to delete Market id.
     * @author Cuneyt YILDIRIM
     */
    public static void deleteMarket(BusinessLogic bl) {
        String marketId = getInput("Enter to delete Market id");
        int retrn;
        try {
            retrn = bl.delMarket(Integer.parseInt(marketId));
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * @description Gets the necessary information to delete RoleId PerformId, ContributorId.
     * @author Cuneyt YILDIRIM
     */
    public static void deleteRole(BusinessLogic bl) {
        String roleid = getInput("Enter to delete Role id");
        String performid = getInput("Enter to delete Perfomance id");
        String contid = getInput("Enter to delete Contributor id");
        int retrn;
        try {
            retrn = bl.delRole(Integer.parseInt(roleid), Integer.parseInt(performid),Integer.parseInt(contid));
            System.out.println(retrn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description Converts String to Date.
     * @author Cuneyt YILDIRIM
     */
    public static Date formatDate(String rawDate){
        return Date.valueOf(rawDate);
    }

    /**
     * @description Gets the user's decision and verifies his answer is in the correct range and is a number.
     * @author Cuneyt YILDIRIM
     */
    public static int getDecisions(int userChoice) {
        Scanner scanner = new Scanner(System.in); 
        int input;
        do {
            System.out.print("---> ");
            try {
                input = Integer.parseInt(scanner.next());
            } catch (Exception e) {
                input = -1;
                System.out.print("Please enter an integer!");
            }
        } while (input < 1 || input > userChoice);
        scanner.close();
        return input;
    }

    /**
     * @description Gets user's answer and return a String.
     * @author Cuneyt YILDIRIM
     * @param Prompted question to get an answer from the user.
     * @return String What the user entered.
     */

    public static String getInput(String question){
        Scanner scanner = new Scanner(System.in);
        String input = null;
        do {
            try {
                System.out.println(question);
                input = scanner.next();
            } catch (Exception e) {
                input = null;
                System.out.println("Please enter a value");
            }

        } while (input == null);
      
       scanner.close();
        return input;
    }


}

