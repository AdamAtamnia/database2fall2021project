

import java.sql.Date;
import java.util.List;

/**
 * Represents a recording (It has no components)
 * @author Adam Atamnia
 */
public class Recording extends APerformance{
    public Recording(int performId, String name, Date creationDate, String duration, List<Contributor> contributors){
        super(performId, name, creationDate, duration, contributors);
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        
        if(!(obj instanceof Recording)){
            return false;
        }

        if (super.equals(obj)) {
            isEqual = true;
        }
        return isEqual;
    }
}
