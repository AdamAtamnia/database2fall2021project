/*
This file has been made by: Adam Atamnia
*/
drop sequence logId_sequence; 
drop sequence distTypeId_sequence; 
drop sequence reclabelId_sequence;
drop sequence marketId_sequence;
drop sequence distId_sequence;
drop sequence performId_sequence;
drop sequence contId_sequence;
drop sequence roleId_sequence;
drop table ActivityLog cascade constraints;
drop table DistributionTypes cascade constraints;
drop table RecordLabel cascade constraints;
drop table Market cascade constraints;
drop table Distribution cascade constraints;
drop table Performance cascade constraints;
drop table Distribution_Performance cascade constraints;
drop table Performance_Component cascade constraints;
drop table Contributor cascade constraints;
drop table Role cascade constraints;
drop table Performance_Contributor_Role cascade constraints;

create sequence logId_sequence
    minvalue 1;
create table ActivityLog (
    logId number(4,0) primary key,
    userName varchar2(20),
    typeChange varchar2(10),
    changedTable varchar2(20),
    message varchar2(200),
    dateTime date
);

insert into ActivityLog values(logId_sequence.nextval, 'sampleUserForTesting', 'fakeDelete', 'fakePerformance', 'fakeMessage1', TO_DATE('2/01/2016', 'MM/DD/YYYY'));
insert into ActivityLog values(logId_sequence.nextval, 'sampleUserForTesting', 'fakeUpdate', 'fakeContributor', 'fakeMessage2', TO_DATE('5/06/2016', 'MM/DD/YYYY'));
insert into ActivityLog values(logId_sequence.nextval, 'sampleUserForTesting', 'fakeInsert', 'fakeRole', 'fakeMessage3', TO_DATE('12/01/2017', 'MM/DD/YYYY'));

create sequence distTypeId_sequence
    minvalue 1;
create table DistributionTypes (
    distTypeId number(4,0) primary key,
    typeName varchar2(20)
);

insert into DistributionTypes values(distTypeId_sequence.nextval, 'Collector Set');
insert into DistributionTypes values(distTypeId_sequence.nextval, 'Long Play');
insert into DistributionTypes values(distTypeId_sequence.nextval, 'Extended Play');
insert into DistributionTypes values(distTypeId_sequence.nextval, 'Single');

create sequence reclabelId_sequence
    minvalue 1;
create table RecordLabel (
    reclabelId number(4,0) primary key,
    recLabelName varchar2(20)
);

insert into RecordLabel values(reclabelId_sequence.nextval, 'YoyoCorp');
insert into RecordLabel values(reclabelId_sequence.nextval, 'AbcCorp');
insert into RecordLabel values(reclabelId_sequence.nextval, 'CoolCorp');

create sequence marketId_sequence
    minvalue 1;
create table Market (
    marketId number(4,0) primary key,
    marketName varchar2(20)
);
insert into Market values(marketId_sequence.nextval, 'EUROPE');
insert into Market values(marketId_sequence.nextval, 'USA');
insert into Market values(marketId_sequence.nextval, 'CANADA');
insert into Market values(marketId_sequence.nextval, 'ASIA');
insert into Market values(marketId_sequence.nextval, 'AFRICA');

create sequence distId_sequence
    minvalue 1;
create table Distribution (
    distId number(4,0) primary key,
    distName varchar2(20),
    --Collector Set, Album (Long Play, LP), Extended Play (EP), Single
    distTypeId number(4,0), 
    releaseDate date,
    recLabelId number(4,0),
    marketId number(4,0),
    constraint FK_distTypeId foreign key (distTypeId) references DistributionTypes(distTypeId),
    constraint FK_reclabelId foreign key (reclabelId) references RecordLabel(reclabelId),
    constraint FK_marketId foreign key (marketId) references Market(marketId)

);

insert into distribution values(distId_sequence.nextval, 'abcDist', 1, TO_DATE('12/01/2018', 'MM/DD/YYYY'), 1, 1);
insert into distribution values(distId_sequence.nextval, '123Dist', 2, TO_DATE('12/01/2022', 'MM/DD/YYYY'), 2, 2);
insert into distribution values(distId_sequence.nextval, 'qwertyDist', 2, TO_DATE('12/01/2017', 'MM/DD/YYYY'), 3, 3);


create sequence performId_sequence
    minvalue 1;  
create table Performance (
    performId number(4,0) primary key,
    performName varchar2(20),
    createdate date,
    duration varchar2(8)
);

insert into Performance values(performId_sequence.nextval, 'HeyComp', TO_DATE('12/01/2017', 'MM/DD/YYYY'), '00:05:00');
insert into Performance values(performId_sequence.nextval, 'HelloComp', TO_DATE('12/01/2010', 'MM/DD/YYYY'), '00:06:00');
insert into Performance values(performId_sequence.nextval, 'GoodbyeComp', TO_DATE('12/01/2011', 'MM/DD/YYYY'), '00:11:00');
insert into Performance values(performId_sequence.nextval, 'HiReco', TO_DATE('12/01/2012', 'MM/DD/YYYY'), '00:05:30');
insert into Performance values(performId_sequence.nextval, 'Comme to me Reco', TO_DATE('12/01/2013', 'MM/DD/YYYY'), '00:07:06');
insert into Performance values(performId_sequence.nextval, 'Stay away Reco', TO_DATE('12/01/2014', 'MM/DD/YYYY'), '01:04:30');
insert into Performance values(performId_sequence.nextval, 'Do Homework Reco', TO_DATE('12/01/2015', 'MM/DD/YYYY'), '00:45:00');

create table Distribution_Performance (
    distId number(4,0),
    performId number(4,0),
    constraint FK_distId foreign key (distID) references Distribution(distId),
    constraint FK_performId foreign key (performId) references Performance(performId)
);

insert into Distribution_Performance values(1, 1);
insert into Distribution_Performance values(1, 2);
insert into Distribution_Performance values(1, 3);
insert into Distribution_Performance values(2, 3);
insert into Distribution_Performance values(2, 4);
insert into Distribution_Performance values(2, 5);
insert into Distribution_Performance values(3, 6);
insert into Distribution_Performance values(3, 7);


create table Performance_Component (
    compilationId number(4,0),
    componentId number(4,0),
    offsetFromBeginning varchar2(10),
    --duration varchar2(10), can cause insert anomalies
    offsetFromSelf varchar2(10),
    durationUsed varchar2(10),
    constraint FK_compilationId foreign key (compilationId) references Performance(performId),
    constraint FK_componentId foreign key (componentId) references Performance(performId)
);

insert into Performance_Component values(1, 7, '00:00:00', '00:00:45', '00:01:00');
insert into Performance_Component values(1, 3, '00:02:00', '00:00:35', '00:02:00');
insert into Performance_Component values(2, 3, '00:00:00', '00:00:10', '00:00:30');
insert into Performance_Component values(2, 4, '00:01:30', '00:02:30', '00:02:00');
insert into Performance_Component values(2, 5, '00:04:30', '00:00:30', '00:01:00');
insert into Performance_Component values(3, 6, '00:00:30', '00:00:30', '00:01:07');
insert into Performance_Component values(3, 7, '00:03:30', '00:02:30', '00:03:00');

create sequence contId_sequence
    minvalue 1;
create table Contributor (
    contId number(4,0) primary key,
    firstName varchar2(20),
    lastName varchar2(20)
);

insert into Contributor values(contId_sequence.nextval, 'Juju', 'Peterson');
insert into Contributor values(contId_sequence.nextval, 'Yop', 'Smith');
insert into Contributor values(contId_sequence.nextval, 'Kyle', 'Wit');
insert into Contributor values(contId_sequence.nextval, 'Kit', 'Blue');
insert into Contributor values(contId_sequence.nextval, 'Cotta', 'Green');
insert into Contributor values(contId_sequence.nextval, 'Zuk', 'Red');

create sequence roleId_sequence
    minvalue 1;
create table Role (
    roleId number(4,0) primary key,
    roleName varchar2(20)
);

insert into Role values(roleId_sequence.nextval, 'Sound Engineer');
insert into Role values(roleId_sequence.nextval, 'Voice');
insert into Role values(roleId_sequence.nextval, 'Motivational Speaker');
insert into Role values(roleId_sequence.nextval, 'Editor');

create table Performance_Contributor_Role (
    performId number(4,0),
    contId number(4,0),
    roleId number(4,0),
    constraint FK_performId_Performance_Contributor foreign key (performId) references Performance(performId),
    constraint FK_contId foreign key (contId) references Contributor(contId),
    constraint FK_roleId foreign key (roleId) references Role(roleId)
);

insert into Performance_Contributor_Role values(1,1,1);
insert into Performance_Contributor_Role values(1,2,2);
insert into Performance_Contributor_Role values(1,3,3);
insert into Performance_Contributor_Role values(2,1,1);
insert into Performance_Contributor_Role values(2,4,1);
insert into Performance_Contributor_Role values(2,5,4);
insert into Performance_Contributor_Role values(3,6,2);
insert into Performance_Contributor_Role values(3,6,4);
insert into Performance_Contributor_Role values(4,2,2);
insert into Performance_Contributor_Role values(4,3,3);
insert into Performance_Contributor_Role values(5,4,2);
insert into Performance_Contributor_Role values(6,5,3);
insert into Performance_Contributor_Role values(7,1,1);




