

/**
 * Represents a component
 * @author Adam Atamnia
 */
public class Component{

    private APerformance performance;
    private String offsetParent;
    private String offsetSelf;
    private String durationUsed;

    public Component(APerformance performance, String offsetParent, String offsetSelf, String durationUsed){
        if( performance == null ||  offsetParent == null || offsetSelf == null ||  durationUsed == null){
            throw new IllegalArgumentException();
        }
        this.performance = performance;
        this.offsetParent = offsetParent;
        this.offsetSelf = offsetSelf;
        this.durationUsed = durationUsed;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        Component component = (Component)obj;
        if (
            this.performance.equals(component.performance) &&
            this.offsetParent.equals(component.offsetParent) &&
            this.offsetSelf.equals(component.offsetSelf) &&
            this.durationUsed.equals(component.durationUsed)
            ) {
            isEqual = true;
        }
        return isEqual;
    }

    @Override
    public String toString() {       
        StringBuilder builder = new StringBuilder("| "+offsetParent+" | "+offsetSelf+" | "+durationUsed+" |\n");
        builder.append("Used Performance:\n");
        builder.append(performance.toString());

        return builder.toString();
    }
}