package tests;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

import org.junit.Test;

import src.APerformance;
import src.ActivityLog;
import src.BusinessLogic;
import src.Compilation;
import src.Component;
import src.Contributor;
import src.Credentials;
import src.Distribution;
import src.Recording;

/**
 * Tests all the methods that get data from the database
 * Make sure to run the BUILD.sql script before every test
 * @author Adam Atamnia
 */
public class testGettingData {

    // Testing all the equals methods from our mirror classes
    @Test
    public void testAllEqualsMethods(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA = {"Motivational Speaker"};
        List<Contributor> contributorsA = new ArrayList<Contributor>();
        contributorsA.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA)));

        String[] rolesB = {"Sound Engineer"};
        List<Contributor> contributorsB = new ArrayList<Contributor>();
        contributorsB.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB)));

        List<Component> components = new ArrayList<Component>();
        components.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA), "00:00:30", "00:00:30", "00:01:07"));
        components.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB), "00:03:30", "00:02:30", "00:03:00"));

        String[] roles1 = {"Voice", "Editor"};
        List<Contributor> contributors1 = new ArrayList<Contributor>();
        contributors1.add(new Contributor(6, "Zuk Red", Arrays.asList(roles1)));

        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};
        List<Contributor> contributors2 = new ArrayList<Contributor>();
        contributors2.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors2.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));

        String[] roles4 = {"Voice"};
        List<Contributor> contributors3 = new ArrayList<Contributor>();
        contributors3.add(new Contributor(4, "Kit Blue", Arrays.asList(roles4)));

        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Compilation(3, "GoodbyeComp", Date.valueOf("2011-12-1"), "00:11:00", contributors1, components));
        performances.add(new Recording(4, "HiReco", Date.valueOf("2012-12-1"), "00:05:30", contributors2));
        performances.add(new Recording(5, "Comme to me Reco", Date.valueOf("2013-12-1"), "00:07:06", contributors3));
        

        Distribution dist = new Distribution(2, "123Dist", "Long Play", Date.valueOf("2022-12-1"), "AbcCorp", "USA", performances);

        String[] rolesA$ = {"Motivational Speaker"};
        List<Contributor> contributorsA$ = new ArrayList<Contributor>();
        contributorsA$.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA$)));

        String[] rolesB$ = {"Sound Engineer"};
        List<Contributor> contributorsB$ = new ArrayList<Contributor>();
        contributorsB$.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB$)));

        List<Component> components$ = new ArrayList<Component>();
        components$.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA$), "00:00:30", "00:00:30", "00:01:07"));
        components$.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB$), "00:03:30", "00:02:30", "00:03:00"));

        String[] roles1$ = {"Voice", "Editor"};
        List<Contributor> contributors1$ = new ArrayList<Contributor>();
        contributors1$.add(new Contributor(6, "Zuk Red", Arrays.asList(roles1$)));

        String[] roles2$ = {"Voice"};
        String[] roles3$ = {"Motivational Speaker"};
        List<Contributor> contributors2$ = new ArrayList<Contributor>();
        contributors2$.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2$)));
        contributors2$.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3$)));

        String[] roles4$ = {"Voice"};
        List<Contributor> contributors3$ = new ArrayList<Contributor>();
        contributors3$.add(new Contributor(4, "Kit Blue", Arrays.asList(roles4$)));

        List<APerformance> performances$ = new ArrayList<APerformance>();
        performances$.add(new Compilation(3, "GoodbyeComp", Date.valueOf("2011-12-1"), "00:11:00", contributors1$, components$));
        performances$.add(new Recording(4, "HiReco", Date.valueOf("2012-12-1"), "00:05:30", contributors2$));
        performances$.add(new Recording(5, "Comme to me Reco", Date.valueOf("2013-12-1"), "00:07:06", contributors3$));
        

        Distribution dist$ = new Distribution(2, "123Dist", "Long Play", Date.valueOf("2022-12-1"), "AbcCorp", "USA", performances$);
        
        try {
            assertEquals(dist, dist$);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }
    
    // Testing getDistribution
    @Test
    public void testGetDistribution(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA = {"Motivational Speaker"};
        List<Contributor> contributorsA = new ArrayList<Contributor>();
        contributorsA.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA)));

        String[] rolesB = {"Sound Engineer"};
        List<Contributor> contributorsB = new ArrayList<Contributor>();
        contributorsB.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB)));

        List<Component> components = new ArrayList<Component>();
        components.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA), "00:00:30", "00:00:30", "00:01:07"));
        components.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB), "00:03:30", "00:02:30", "00:03:00"));

        String[] roles1 = {"Voice", "Editor"};
        List<Contributor> contributors1 = new ArrayList<Contributor>();
        contributors1.add(new Contributor(6, "Zuk Red", Arrays.asList(roles1)));

        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};
        List<Contributor> contributors2 = new ArrayList<Contributor>();
        contributors2.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors2.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));

        String[] roles4 = {"Voice"};
        List<Contributor> contributors3 = new ArrayList<Contributor>();
        contributors3.add(new Contributor(4, "Kit Blue", Arrays.asList(roles4)));

        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Compilation(3, "GoodbyeComp", Date.valueOf("2011-12-1"), "00:11:00", contributors1, components));
        performances.add(new Recording(4, "HiReco", Date.valueOf("2012-12-1"), "00:05:30", contributors2));
        performances.add(new Recording(5, "Comme to me Reco", Date.valueOf("2013-12-1"), "00:07:06", contributors3));
        

        Distribution dist = new Distribution(2, "123Dist", "Long Play", Date.valueOf("2022-12-1"), "AbcCorp", "USA", performances);
        
        try {
            Distribution myDist = bl.getDistribution(2);
            assertEquals(dist, myDist);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetDistributionIllegalArgument(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getDistribution(100);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    // Testing getPerformancesFromDistribution
    @Test
    public void testGetPerformancesFromDistribution(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA = {"Motivational Speaker"};
        List<Contributor> contributorsA = new ArrayList<Contributor>();
        contributorsA.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA)));

        String[] rolesB = {"Sound Engineer"};
        List<Contributor> contributorsB = new ArrayList<Contributor>();
        contributorsB.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB)));

        List<Component> components = new ArrayList<Component>();
        components.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA), "00:00:30", "00:00:30", "00:01:07"));
        components.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB), "00:03:30", "00:02:30", "00:03:00"));

        String[] roles1 = {"Voice", "Editor"};
        List<Contributor> contributors1 = new ArrayList<Contributor>();
        contributors1.add(new Contributor(6, "Zuk Red", Arrays.asList(roles1)));

        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};
        List<Contributor> contributors2 = new ArrayList<Contributor>();
        contributors2.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors2.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));

        String[] roles4 = {"Voice"};
        List<Contributor> contributors3 = new ArrayList<Contributor>();
        contributors3.add(new Contributor(4, "Kit Blue", Arrays.asList(roles4)));

        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Compilation(3, "GoodbyeComp", Date.valueOf("2011-12-1"), "00:11:00", contributors1, components));
        performances.add(new Recording(4, "HiReco", Date.valueOf("2012-12-1"), "00:05:30", contributors2));
        performances.add(new Recording(5, "Comme to me Reco", Date.valueOf("2013-12-1"), "00:07:06", contributors3));
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromDistribution(2);
            assertEquals(performancesR, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromDistributionIllegalArgument(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getPerformancesFromDistribution(100);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    // Testing getPerformance (using a compilation, recording and illegal argument)
    @Test
    public void testGetPerformanceCompilation(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA = {"Motivational Speaker"};
        List<Contributor> contributorsA = new ArrayList<Contributor>();
        contributorsA.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA)));

        String[] rolesB = {"Sound Engineer"};
        List<Contributor> contributorsB = new ArrayList<Contributor>();
        contributorsB.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB)));

        List<Component> components = new ArrayList<Component>();
        components.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA), "00:00:30", "00:00:30", "00:01:07"));
        components.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB), "00:03:30", "00:02:30", "00:03:00"));

        String[] roles1 = {"Voice", "Editor"};
        List<Contributor> contributors1 = new ArrayList<Contributor>();
        contributors1.add(new Contributor(6, "Zuk Red", Arrays.asList(roles1)));
        
        try {
            APerformance performanceR = bl.getPerformance(3);
            assertEquals(new Compilation(3, "GoodbyeComp", Date.valueOf("2011-12-1"), "00:11:00", contributors1, components), performanceR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformanceRecording(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA = {"Motivational Speaker"};
        List<Contributor> contributorsA = new ArrayList<Contributor>();
        contributorsA.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA)));
        
        try {
            APerformance performanceR = bl.getPerformance(6);
            assertEquals(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA), performanceR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }
    
    @Test
    public void testGetPerformanceIllegalArgument(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getPerformance(100);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    // Testing getAllContributorsAndRoles
    @Test
    public void testGetAllContributorsAndRoles(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};
        List<Contributor> contributors2 = new ArrayList<Contributor>();
        contributors2.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors2.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));
        
        try {
            List<Contributor> contributors = bl.getAllContributorsAndRoles(4);
            assertEquals(contributors2, contributors);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetAllContributorsAndRolesIllegalArgument(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getAllContributorsAndRoles(100);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    // Testing getAllRoles
    @Test
    public void testGetAllRoles(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] roles = {"Motivational Speaker", "Editor"};
        
        try {
            List<String> rolesR = bl.getAllRoles(5);
            assertEquals(Arrays.asList(roles), rolesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetAllRolesIllegalArgument(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getAllRoles(100);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    // Testing getContributorRolesFromPerformance (If the illegal argument is the first or the second or both)
    @Test
    public void testGetContributorRolesFromPerformance(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] roles = {"Sound Engineer"};
        
        try {
            List<String> rolesR = bl.getContributorRolesFromPerformance(2, 4);
            assertEquals(Arrays.asList(roles), rolesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetContributorRolesFromPerformanceIllegalArgument1(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getContributorRolesFromPerformance(100, 4);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetContributorRolesFromPerformanceIllegalArgument2(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getContributorRolesFromPerformance(2, 100);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetContributorRolesFromPerformanceIllegalArgumentBoth(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getContributorRolesFromPerformance(100, 100);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    // Testing getAllComponents
    @Test
    public void testGetAllComponents(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA = {"Motivational Speaker"};
        List<Contributor> contributorsA = new ArrayList<Contributor>();
        contributorsA.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA)));

        String[] rolesB = {"Sound Engineer"};
        List<Contributor> contributorsB = new ArrayList<Contributor>();
        contributorsB.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB)));

        List<Component> components = new ArrayList<Component>();
        components.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA), "00:00:30", "00:00:30", "00:01:07"));
        components.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB), "00:03:30", "00:02:30", "00:03:00"));
        
        try {
            List<Component> componentsR = bl.getAllComponents(3);
            assertEquals(components, componentsR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetAllComponentsIllegalArgument(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getAllComponents(100);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    // Testing getPerformancesFromContributor (with filters)
    @Test
    public void testGetPerformancesFromContributorNoFilters(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA3 = {"Motivational Speaker"};
        List<Contributor> contributorsA3 = new ArrayList<Contributor>();
        contributorsA3.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA3)));

        String[] rolesB3 = {"Sound Engineer"};
        List<Contributor> contributorsB3 = new ArrayList<Contributor>();
        contributorsB3.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB3)));

        List<Component> components3 = new ArrayList<Component>();
        components3.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA3), "00:00:30", "00:00:30", "00:01:07"));
        components3.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB3), "00:03:30", "00:02:30", "00:03:00"));

        String[] roles31 = {"Voice", "Editor"};
        List<Contributor> contributors3 = new ArrayList<Contributor>();
        contributors3.add(new Contributor(6, "Zuk Red", Arrays.asList(roles31)));


        String[] roles7 = {"Sound Engineer"};
        List<Contributor> contributors7 = new ArrayList<Contributor>();
        contributors7.add(new Contributor(1, "Juju Peterson", Arrays.asList(roles7)));

        List<Component> components1 = new ArrayList<Component>();
        components1.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributors7), "00:00:00", "00:00:45", "00:01:00"));
        components1.add(new Component(new Compilation(3, "GoodbyeComp", Date.valueOf("2011-12-1"), "00:11:00", contributors3, components3), "00:02:00", "00:00:35", "00:02:00"));

        String[] roles1 = {"Sound Engineer"};
        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};
        List<Contributor> contributors1 = new ArrayList<Contributor>();
        contributors1.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors1.add(new Contributor(1, "Juju Peterson", Arrays.asList(roles1)));
        contributors1.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));

        List<Contributor> contributors2 = new ArrayList<Contributor>();
        contributors2.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors2.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));

        
        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Compilation(1, "HeyComp", Date.valueOf("2017-12-1"), "00:05:00", contributors1, components1));
        performances.add(new Recording(4, "HiReco", Date.valueOf("2012-12-1"), "00:05:30", contributors2));
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromContributor(2, false, false, false, false);
            assertEquals(performances, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorCompilationOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA3 = {"Motivational Speaker"};
        List<Contributor> contributorsA3 = new ArrayList<Contributor>();
        contributorsA3.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA3)));

        String[] rolesB3 = {"Sound Engineer"};
        List<Contributor> contributorsB3 = new ArrayList<Contributor>();
        contributorsB3.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB3)));

        List<Component> components3 = new ArrayList<Component>();
        components3.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA3), "00:00:30", "00:00:30", "00:01:07"));
        components3.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB3), "00:03:30", "00:02:30", "00:03:00"));

        String[] roles31 = {"Voice", "Editor"};
        List<Contributor> contributors3 = new ArrayList<Contributor>();
        contributors3.add(new Contributor(6, "Zuk Red", Arrays.asList(roles31)));


        String[] roles7 = {"Sound Engineer"};
        List<Contributor> contributors7 = new ArrayList<Contributor>();
        contributors7.add(new Contributor(1, "Juju Peterson", Arrays.asList(roles7)));

        List<Component> components1 = new ArrayList<Component>();
        components1.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributors7), "00:00:00", "00:00:45", "00:01:00"));
        components1.add(new Component(new Compilation(3, "GoodbyeComp", Date.valueOf("2011-12-1"), "00:11:00", contributors3, components3), "00:02:00", "00:00:35", "00:02:00"));

        String[] roles1 = {"Sound Engineer"};
        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};
        List<Contributor> contributors1 = new ArrayList<Contributor>();
        contributors1.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors1.add(new Contributor(1, "Juju Peterson", Arrays.asList(roles1)));
        contributors1.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));
        
        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Compilation(1, "HeyComp", Date.valueOf("2017-12-1"), "00:05:00", contributors1, components1));
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromContributor(2, false, false, true, false);
            assertEquals(performances, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorRecordingOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};

        List<Contributor> contributors2 = new ArrayList<Contributor>();
        contributors2.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors2.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));

        
        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Recording(4, "HiReco", Date.valueOf("2012-12-1"), "00:05:30", contributors2));
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromContributor(2, false, false, false, true);
            assertEquals(performances, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorReleasedOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA3 = {"Motivational Speaker"};
        List<Contributor> contributorsA3 = new ArrayList<Contributor>();
        contributorsA3.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA3)));

        String[] rolesB3 = {"Sound Engineer"};
        List<Contributor> contributorsB3 = new ArrayList<Contributor>();
        contributorsB3.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB3)));

        List<Component> components3 = new ArrayList<Component>();
        components3.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA3), "00:00:30", "00:00:30", "00:01:07"));
        components3.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB3), "00:03:30", "00:02:30", "00:03:00"));

        String[] roles31 = {"Voice", "Editor"};
        List<Contributor> contributors3 = new ArrayList<Contributor>();
        contributors3.add(new Contributor(6, "Zuk Red", Arrays.asList(roles31)));


        String[] roles7 = {"Sound Engineer"};
        List<Contributor> contributors7 = new ArrayList<Contributor>();
        contributors7.add(new Contributor(1, "Juju Peterson", Arrays.asList(roles7)));

        List<Component> components1 = new ArrayList<Component>();
        components1.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributors7), "00:00:00", "00:00:45", "00:01:00"));
        components1.add(new Component(new Compilation(3, "GoodbyeComp", Date.valueOf("2011-12-1"), "00:11:00", contributors3, components3), "00:02:00", "00:00:35", "00:02:00"));

        String[] roles1 = {"Sound Engineer"};
        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};
        List<Contributor> contributors1 = new ArrayList<Contributor>();
        contributors1.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors1.add(new Contributor(1, "Juju Peterson", Arrays.asList(roles1)));
        contributors1.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));
        
        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Compilation(1, "HeyComp", Date.valueOf("2017-12-1"), "00:05:00", contributors1, components1));
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromContributor(2, true, false, false, false);
            assertEquals(performances, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorNotReleasedOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};

        List<Contributor> contributors2 = new ArrayList<Contributor>();
        contributors2.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors2.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));

        
        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Recording(4, "HiReco", Date.valueOf("2012-12-1"), "00:05:30", contributors2));
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromContributor(2, false, false, false, true);
            assertEquals(performances, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorReleasedOnlyAndCompilationOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String[] rolesA3 = {"Motivational Speaker"};
        List<Contributor> contributorsA3 = new ArrayList<Contributor>();
        contributorsA3.add(new Contributor(5, "Cotta Green", Arrays.asList(rolesA3)));

        String[] rolesB3 = {"Sound Engineer"};
        List<Contributor> contributorsB3 = new ArrayList<Contributor>();
        contributorsB3.add(new Contributor(1, "Juju Peterson", Arrays.asList(rolesB3)));

        List<Component> components3 = new ArrayList<Component>();
        components3.add(new Component(new Recording(6, "Stay away Reco", Date.valueOf("2014-12-1"), "01:04:30", contributorsA3), "00:00:30", "00:00:30", "00:01:07"));
        components3.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributorsB3), "00:03:30", "00:02:30", "00:03:00"));

        String[] roles31 = {"Voice", "Editor"};
        List<Contributor> contributors3 = new ArrayList<Contributor>();
        contributors3.add(new Contributor(6, "Zuk Red", Arrays.asList(roles31)));


        String[] roles7 = {"Sound Engineer"};
        List<Contributor> contributors7 = new ArrayList<Contributor>();
        contributors7.add(new Contributor(1, "Juju Peterson", Arrays.asList(roles7)));

        List<Component> components1 = new ArrayList<Component>();
        components1.add(new Component(new Recording(7, "Do Homework Reco", Date.valueOf("2015-12-1"), "00:45:00", contributors7), "00:00:00", "00:00:45", "00:01:00"));
        components1.add(new Component(new Compilation(3, "GoodbyeComp", Date.valueOf("2011-12-1"), "00:11:00", contributors3, components3), "00:02:00", "00:00:35", "00:02:00"));

        String[] roles1 = {"Sound Engineer"};
        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};
        List<Contributor> contributors1 = new ArrayList<Contributor>();
        contributors1.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors1.add(new Contributor(1, "Juju Peterson", Arrays.asList(roles1)));
        contributors1.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));
        
        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Compilation(1, "HeyComp", Date.valueOf("2017-12-1"), "00:05:00", contributors1, components1));
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromContributor(2, true, false, true, false);
            assertEquals(performances, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorReleasedOnlyAndRecordingOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        List<APerformance> performances = new ArrayList<APerformance>();
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromContributor(2, true, false, false, true);
            assertEquals(performances, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorNotReleasedOnlyAndCompilationOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        List<APerformance> performances = new ArrayList<APerformance>();
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromContributor(2, false, true, true, false);
            assertEquals(performances, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorNotReleasedOnlyAndRecordingOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        String[] roles2 = {"Voice"};
        String[] roles3 = {"Motivational Speaker"};

        List<Contributor> contributors2 = new ArrayList<Contributor>();
        contributors2.add(new Contributor(2, "Yop Smith", Arrays.asList(roles2)));
        contributors2.add(new Contributor(3, "Kyle Wit", Arrays.asList(roles3)));

        
        List<APerformance> performances = new ArrayList<APerformance>();
        performances.add(new Recording(4, "HiReco", Date.valueOf("2012-12-1"), "00:05:30", contributors2));
        
        try {
            List<APerformance> performancesR = bl.getPerformancesFromContributor(2, false, true, false, true);
            assertEquals(performances, performancesR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorIllegalArgumentContributorId(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getPerformancesFromContributor(100, false, true, false, true);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorIllegalArgumentReleasedOnlyAndNotReleasedOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getPerformancesFromContributor(100, true, true, false, true);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorIllegalArgumentCompilationOnlyAndRecordingOnly(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getPerformancesFromContributor(100, false, true, true, true);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetPerformancesFromContributorIllegalArgumentAllContradictingFilters(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        
        try {
            bl.getPerformancesFromContributor(100, true, true, true, true);
            fail("Did not catch illegal argument exception");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    // Testing getActivityLogs
    @Test
    public void testGetActivityLogs(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        List<ActivityLog> activityLogs = new ArrayList<ActivityLog>();
        activityLogs.add(new ActivityLog(1, "sampleUserForTesting", "fakeDelete", "fakePerformance", "fakeMessage1",  Date.valueOf("2016-2-1")));
        activityLogs.add(new ActivityLog(2, "sampleUserForTesting", "fakeUpdate", "fakeContributor", "fakeMessage2",  Date.valueOf("2016-5-6")));
        
        try {
            List<ActivityLog> activityLogsR = bl.getActivityLogs(Date.valueOf("2016-1-1"), Date.valueOf("2016-12-1"));
            assertEquals(activityLogs, activityLogsR);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetActivityLogsIllegalArguments(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            bl.getActivityLogs(null, Date.valueOf("2016-12-1"));
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetActivityLogsIllegalArguments2(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            bl.getActivityLogs(Date.valueOf("2016-12-1"), null);
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetActivityLogsIllegalArgumentsDatesInWrongOrder(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            bl.getActivityLogs(Date.valueOf("2018-12-1"), Date.valueOf("2016-12-1"));
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }
    
    @Test
    public void testGetActivityLogsIllegalArgumentsNoActivityLogFound(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            bl.getActivityLogs(Date.valueOf("1999-12-1"), Date.valueOf("2000-12-1"));
            fail("Expected IllegalArgumentException");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }
    
    // test getAll: Distributions, performances and contributors
    @Test
    public void testGetAllDistributions(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String string = "| Id | Name | Release Date | Type | Record Label | Market |\n| 1 | abcDist | 2018-12-01 | Collector Set | YoyoCorp | EUROPE |\n| 2 | 123Dist | 2022-12-01 | Long Play | AbcCorp | USA |\n| 3 | qwertyDist | 2017-12-01 | Long Play | CoolCorp | CANADA |\n";

        try {
            assertEquals(string, bl.getAllDistributions());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetAllPerformances(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String string = "| Id | Name | Creation Date | Duration |\n| 1 | HeyComp | 2017-12-01 | 00:05:00 |\n| 2 | HelloComp | 2010-12-01 | 00:06:00 |\n| 3 | GoodbyeComp | 2011-12-01 | 00:11:00 |\n| 4 | HiReco | 2012-12-01 | 00:05:30 |\n| 5 | Comme to me Reco | 2013-12-01 | 00:07:06 |\n| 6 | Stay away Reco | 2014-12-01 | 01:04:30 |\n| 7 | Do Homework Reco | 2015-12-01 | 00:45:00 |\n";

        try {
            assertEquals(string, bl.getAllPerformances());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetAllContributorsFromTable(){
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        String string = "| Id | First Name | Last Name |\n| 1 | Juju | Peterson |\n| 2 | Yop | Smith |\n| 3 | Kyle | Wit |\n| 4 | Kit | Blue |\n| 5 | Cotta | Green |\n| 6 | Zuk | Red |\n";

        try {
            assertEquals(string, bl.getAllContributorsFromTable());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

}