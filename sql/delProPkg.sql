CREATE or REPLACE PACKAGE delProPkg is
    PROCEDURE delDistribution(p_distid in Distribution.distid%type, p_deleted out varchar2);
    PROCEDURE delSinglePerformance(p_performid in PERFORMANCE.performid%type, p_deleted out varchar2);
    PROCEDURE delContributor(p_contid CONTRIBUTOR.contid%type, p_roleid ROLE.roleid%type, p_deleted out varchar2);
    PROCEDURE delComponent(p_performId PERFORMANCE.PERFORMID%type, p_deleted out varchar2);
    PROCEDURE delRecordLabel(p_recordId in RECORDLABEL.reclabelId%type, p_deleted out varchar2);
    PROCEDURE delMarket(p_marketId in MARKET.marketId%type, p_deleted out varchar2);
    PROCEDURE delRole( p_roleId in ROLE.roleId%type, p_performId Performance_contributor_role.performId%type, p_contId Performance_contributor_role.contid%type, p_deleted out varchar2);
    PROCEDURE delDistributionType(p_distTypeId in DistributionTypes.distTypeId%type, p_deleted out varchar2);
    FUNCTION doesDistributionExist(p_distid in Distribution.distid%type) RETURN NUMBER;
    FUNCTION doesPerformanceExist(p_performanceId in PERFORMANCE.PERFORMID%type) RETURN NUMBER;
    FUNCTION doesContributorExistPerformance(p_contid CONTRIBUTOR.contid%type) RETURN NUMBER;
    FUNCTION doesContributorExist(p_contid CONTRIBUTOR.contid%type) RETURN NUMBER;
    FUNCTION doesComponentExist(p_performId PERFORMANCE.PERFORMID%type) RETURN NUMBER;
    FUNCTION doesRecordLabelExist(p_recordId in RECORDLABEL.reclabelId%type) RETURN NUMBER;
    FUNCTION doesMarketExist(p_marketId in MARKET.marketId%type) RETURN NUMBER;
    FUNCTION doesRoleExist(p_roleId in ROLE.roleId%type) RETURN NUMBER;
    FUNCTION doesRoleExistPerformance(p_roleId in ROLE.roleId%type) RETURN NUMBER;
    FUNCTION doesDistributionTypeExist(p_distTypedId in DistributionTypes.typeName%type) RETURN NUMBER;
END delProPkg;
/
CREATE or REPLACE PACKAGE BODY delProPkg AS
    /**
     Delete distribution based on the given parameter
      @author Cuneyt YILDIRIM
     */
    PROCEDURE delDistribution(p_distid in Distribution.distid%type, p_deleted out varchar2) IS
        v_isExist number(4, 2);
    BEGIN
        v_isExist := doesDistributionExist(p_distid);
        if v_isExist = 1 then
            DELETE
            from distribution_performance
            WHERE distid = p_distid;
            DELETE
            from distribution
            WHERE distid = p_distid;
            p_deleted := 1;
        else
            p_deleted := 0;
        end if;
    END delDistribution;
    /**
     Deletes Performance based on the given parameters.
     */
    PROCEDURE delSinglePerformance(p_performid in PERFORMANCE.performid%type, p_deleted out varchar2)
        is
        v_isExist number(4, 2);
    begin
        v_isExist := doesPerformanceExist(p_performid);
        if v_isExist = 1 then
            delete from PERFORMANCE_CONTRIBUTOR_ROLE
            where PERFORMID = p_performid;

            delete from PERFORMANCE_COMPONENT
            where COMPILATIONID = p_performid or COMPONENTID = p_performid;

            DELETE
            from PERFORMANCE
            WHERE performid = p_performid;

            p_deleted := 1;
        else
            p_deleted := 0;
        end if;
    end delSinglePerformance;

    /**
    Deletes Contributor based on the given parameter.
     */
    PROCEDURE delContributor(p_contid CONTRIBUTOR.contid%type, p_roleid ROLE.roleid%type, p_deleted out varchar2)
        is
        v_isExist number(4, 2);
        v_isExist2 number(4,2);
    begin
        v_isExist := doesContributorExistPerformance(p_contid);
        v_isExist2 := doesRoleExistPerformance(p_roleid);
        if v_isExist = 1 and v_isExist2 = 1 then
            DELETE
            from PERFORMANCE_CONTRIBUTOR_ROLE
            WHERE CONTID = p_contid
              and roleid = p_roleid;
            p_deleted := 1;
        else
            p_deleted := 0;
        end if;
    END delContributor;

    /**
    Deletes Component based on the given parameters
     */
    PROCEDURE delComponent(p_performId PERFORMANCE.PERFORMID%type, p_deleted out varchar2)
        is
        v_isExist number(4, 2);
    begin
        v_isExist := doesComponentExist(p_performId);
        if v_isExist = 1 then
            Delete from PERFORMANCE_COMPONENT
            where COMPONENTID = p_performId or COMPILATIONID = p_performId;
            p_deleted := 1;
        else
            p_deleted := 0;
        end if;
    end delComponent;

    /**
        Deletes Records label based on the given parameter.
     */
    PROCEDURE delRecordLabel(p_recordId in RECORDLABEL.reclabelId%type, p_deleted out varchar2)
        is
        v_isExist number(4, 2);
    begin
        v_isExist := doesRecordLabelExist(p_recordId);
        if v_isExist = 1 then
            DELETE FROM RECORDLABEL
            where RECLABELID = p_recordId;
            p_deleted := 1;
        elsif v_isExist = -1 then
            p_deleted := -1;
        else
            p_deleted := 0;
        end if;
    end delRecordLabel;

    /**
        Deletes Market from database based on the given parameter.
     */
    PROCEDURE delMarket(p_marketId in MARKET.marketId%type, p_deleted out varchar2)
        is
        v_isExist number(4, 2);
    begin
        v_isExist := doesMarketExist(p_marketId);
        if v_isExist = 1 then
            delete from DISTRIBUTION
            where MARKETID = p_marketId;
            DELETE FROM MARKET
            where MARKETID =p_marketId;
            p_deleted := 1;
        elsif v_isExist = -1 then
            p_deleted := -1;
        else
            p_deleted := 0;
        end if;
    end delMarket;

    /**
        Deletes Role from database based on the given parameter.
     */
    PROCEDURE delRole( p_roleId in ROLE.roleId%type, p_performId Performance_contributor_role.performId%type, p_contId Performance_contributor_role.contid%type, p_deleted out varchar2)
        is
        v_isExist number(4, 2);
    begin
        v_isExist := doesRoleExist(p_roleId);
        if v_isExist = 1 then
            DELETE
            from Performance_contributor_role
            where performId = p_performId;
            DELETE
            from Performance_contributor_role
            where contid = p_contId;
            DELETE
            from Performance_contributor_role
            where roleid = p_roleid;
            DELETE
            from ROLE
            WHERE ROLEID = p_roleid;
            p_deleted := 1;
        else
            p_deleted := 0;
        end if;
    end delRole;

    /**
        Deletes Distribution Type from the database based on the given parameter.
     */
    PROCEDURE delDistributionType(p_distTypeId in DistributionTypes.distTypeId%type, p_deleted out varchar2) is
        v_isExist number(4, 2);
    begin
        v_isExist := doesDistributionTypeExist(p_distTypeId);
        if v_isExist = 1 then
            delete from DISTRIBUTION
            where DISTTYPEID = p_distTypeId;

            Delete from DISTRIBUTIONTYPES
            where DISTTYPEID = p_distTypeId;
            p_deleted := 1;
        else
            p_deleted := 0;
        end if;
    end delDistributionType;

    ---- FUNCTIONS --------

    /**
      Finds a distribution in database based on the given parameter.
     */
    FUNCTION doesDistributionExist(p_distid in Distribution.distid%type)
        RETURN NUMBER
        IS
        v_isExist    number(4, 2);
        v_totalCount number(4, 2);
    BEGIN
        Select count(*) into v_totalCount
        from DISTRIBUTION
        where distid = p_distid;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    END doesDistributionExist;

    /**
        Finds a Performance in database based on the given parameter.
     */
    FUNCTION doesPerformanceExist(p_performanceId in PERFORMANCE.PERFORMID%type)
        RETURN NUMBER
        IS
        v_isExist    number(4, 2);
        v_totalCount number(4, 2);
    begin
        SELECT count(*) into v_totalCount
        FROM PERFORMANCE
        where PERFORMID = p_performanceId;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesPerformanceExist;

    /**
        Finds a Contributor in database based on the given parameter.
     */
    FUNCTION doesContributorExist(p_contid CONTRIBUTOR.contid%type)
        RETURN NUMBER
        IS
        v_isExist    number(4, 2);
        v_totalCount NUMBER(4, 2);
    BEGIN
        SELECT count(*)
        into v_totalCount
        FROM CONTRIBUTOR
        WHERE contid = p_contid;

        if v_totalCount >= 1 THEN v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    END doesContributorExist;
    /**
        Finds a Contributor in database based on the given parameter.
     */
    FUNCTION doesContributorExistPerformance(p_contid CONTRIBUTOR.contid%type)
        RETURN NUMBER
        IS
        v_isExist    number(4, 2);
        v_totalCount NUMBER(4, 2);
    BEGIN
        SELECT count(*)
        into v_totalCount
        FROM PERFORMANCE_CONTRIBUTOR_ROLE
        WHERE contid = p_contid;

        if v_totalCount >= 1 THEN v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    END doesContributorExistPerformance;

    /**
        Finds a Component in database based on the given parameter.
     */
    FUNCTION doesComponentExist(p_performId PERFORMANCE.PERFORMID%type)
        RETURN NUMBER
        is
        v_isExist    number(4, 2);
        v_totalCount NUMBER(4, 2);
    BEGIN
        SELECT count(*)
        into v_totalCount
        FROM PERFORMANCE_COMPONENT
        WHERE COMPILATIONID = p_performId or COMPONENTID = p_performId;
        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesComponentExist;

    /**
        Finds a Record Label in database based on the given parameter.
     */
    FUNCTION doesRecordLabelExist(p_recordId in RECORDLABEL.reclabelId%type)
        RETURN NUMBER
        IS
        v_isExist number(4, 2);
        v_totalCount number(4, 2);
        v_isConnection number(4,2);
    begin
        SELECT count(*)  into v_totalCount
        FROM RECORDLABEL
        where reclabelId = p_recordId;

        select count(*) into v_isConnection
        from DISTRIBUTION
        where RECLABELID = p_recordId;

        if v_isConnection >=1 THEN
            v_isExist := -1;
        elsif v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesRecordLabelExist;

    /**
      Finds a Market in database based on the given parameter.
     */
    FUNCTION doesMarketExist(p_marketId in MARKET.marketId%type)
        RETURN NUMBER
        is
        v_isExist number(4, 2);
        v_totalCount number(4, 2);
        v_isConnection number(4,2);
    begin
        SELECT count(*) into v_totalCount
        FROM MARKET
        where MARKETID = p_marketId;

        select count(*) into v_isConnection
        from DISTRIBUTION
            where MARKETID = p_marketId;
        if v_isConnection >=1 THEN
           v_isExist := -1;
        elsif v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesMarketExist;

    /**
      Finds a Role in database based on the given parameter.
     */
    FUNCTION doesRoleExist(p_roleId in ROLE.roleId%type)
        RETURN NUMBER
        IS
        v_isExist number(4, 2);
        v_totalCount number(4, 2);
    begin
        SELECT count(*)
        into v_totalCount
        FROM ROLE
        where roleId = p_roleId;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesRoleExist;

    /**
Finds a Role in database based on the given parameter.
*/
    FUNCTION doesRoleExistPerformance(p_roleId in ROLE.roleId%type)
        RETURN NUMBER
        IS
        v_isExist number(4, 2);
        v_totalCount number(4, 2);
    begin
        SELECT count(*)
        into v_totalCount
        FROM ROLE
        where roleId = p_roleId;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesRoleExistPerformance;


    FUNCTION doesDistributionTypeExist(p_distTypedId in DistributionTypes.typeName%type)
        RETURN NUMBER
        is
        v_isExist number(4, 2);
        v_totalCount number(4, 2);
    begin
        SELECT count(*)
        into v_totalCount
        FROM DistributionTypes
        where DISTTYPEID = p_distTypedId;

        if v_totalCount >= 1 THEN
            v_isExist := 1;
        ELSE
            v_isExist := 0;
        end if;
        return v_isExist;
    end doesDistributionTypeExist;

END delProPkg;
/