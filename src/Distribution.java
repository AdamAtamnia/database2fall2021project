

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents a distribution and its performances
 * @author Adam Atamnia
 */
public class Distribution {
    private int distId;
    private String distName;
    private String type;
    private Date releaseDate;
    private String recordLabel;
    private String market;
    private List<APerformance> performances;
    
    public Distribution(int distId, String distName, String type, Date releaseDate, String recordLabel,
            String market, List<APerformance> performances) {
        
        if(  distName == null ||  type == null || releaseDate == null ||  recordLabel == null ||
        market == null ||  performances == null){
            throw new IllegalArgumentException();
        }
        
        this.distId = distId;
        this.distName = distName;
        this.type = type;
        this.releaseDate = releaseDate;
        this.recordLabel = recordLabel;
        this.market = market;
        this.performances = new ArrayList<APerformance>(performances);
    }
   
    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        Distribution dist = (Distribution)obj;
        if (
            this.distId == dist.distId &&
            this.distName.equals(dist.distName) &&
            this.type.equals(dist.type) &&
            this.releaseDate.equals(dist.releaseDate) &&
            this.recordLabel.equals(dist.recordLabel) &&
            this.market.equals(dist.market) &&
            this.performances.equals(dist.performances)
            ) {
            isEqual = true;
        }
        return isEqual;
    }

    /**
     * Returns formatted string that shows All Performances Without Contributors And Roles and components
     * @return String
     */
    public String showAllPerformancesWithoutContributorsAndRoles() {       
        StringBuilder builder = new StringBuilder();
        builder.append("Performances of distribution "+distName+":\n");
        builder.append("| Id | Name | Duration | Creation Date |\n");
        for (APerformance performance : performances) {
            builder.append(performance.showAllExceptContributorsAndRoles()+"\n");
        } 

        return builder.toString();
    }

    /**
     * Returns formatted string that shows All possible information
     * @return String
     */
    @Override
    public String toString() {       
        StringBuilder builder = new StringBuilder("| Id | Name | Type | Release Date | Record Label | Market |\n");
        builder.append("| "+distId+" | "+distName+" | "+type+" | "+releaseDate+" | "+recordLabel+" | "+market+" |\n");
        builder.append("**************************************************************\n\n");
        builder.append("Performances of distribution "+distName+":\n");
        builder.append("| Id | Name | Duration | Creation Date |\n");
        for (APerformance performance : performances) {
            builder.append("-------------------------Performance in "+distName+"------------------------------------\n");
            builder.append(performance.toString()+"\n");
        } 

        return builder.toString();
    }
}
