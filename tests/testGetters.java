package tests;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.Arrays;

import org.junit.Test;

import src.BusinessLogic;
import src.Contributor;
import src.Credentials;

public class testGetters {
    @Test
    public void testGetNameInAPerformance(){
        String string = "HeyComp";
        
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            assertEquals(string, bl.getPerformance(1).getName());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    }

    @Test
    public void testGetContIdInContributor(){
        
        String[] rolesA = {"Motivational Speaker"};
        Contributor contributor = new Contributor(5, "Cotta Green", Arrays.asList(rolesA));
        
        assertEquals(5, contributor.getContId());
    }
}
