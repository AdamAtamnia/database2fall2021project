/*
The GetData Package has been authored by: Adam Atamnia 
*/
create or replace package GetData is
    function getAllPerformances return SYS_REFCURSOR;
    function getAllDistributions return SYS_REFCURSOR;
    function getAllContributorsFromTable return SYS_REFCURSOR;
    function getActivityLogs(pStartDate in activityLog.dateTime%type, pEndDate in activityLog.dateTime%type, pIsValid out number) return SYS_REFCURSOR;
    function getPerformanceDetails(pPerformId in performance.performId%type, pIsExisting out number) return SYS_REFCURSOR;
    function getPerformancesFromContributor(pContid in contributor.contid%type, pIsExisting out number) return SYS_REFCURSOR;
    function getAllComponents(pPerformId in performance.performId%type, pIsExisting out number) return SYS_REFCURSOR;
    function getAllContributors(pPerformId in performance.performId%type, pIsExisting out number) return SYS_REFCURSOR;
    function getContributorRolesFromPerformance(
        pPerformId in performance.performId%type, 
        pContId in CONTRIBUTOR.contId%type, 
        pIsExisting out number) return SYS_REFCURSOR;
    function getAllRoles(pContId in CONTRIBUTOR.contId%type, pIsExisting out number) return SYS_REFCURSOR;
    function getDistributionDetails(pDistId in DISTRIBUTION.distId%type, pIsExisting out number) return SYS_REFCURSOR;
    function getDistributionsFromPerformance(pPerformId in PERFORMANCE.performId%type, pIsExisting out number) return SYS_REFCURSOR;
    function getPerformancesFromDistribution(pDistId in Distribution.distid%type, pIsExisting out number) return SYS_REFCURSOR;

    function isCompilation(pPerformId in performance.performId%type, pIsExisting out number) return number;
    function isReleased(pPerformId in performance.performId%type, pIsExisting out number) return number;
    function doesPerformanceExist(pPerformId in performance.performId%type) return number;
    function doesDistributionExist(pDistId in distribution.distId%type) return number;
    function doesContributorExist(pContId in contributor.contId%type) return number;

end GetData;
/

create or replace package body GetData is
    /*
    Gets all the performances of the database
    returns a cursor 
    */
    function getAllPerformances
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin

        open vResult for
        select * from performance;
        
        return vResult;
    end;
    

    /*
    Gets all the distributions of the database
    returns a cursor 
    */
    function getAllDistributions
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin

        open vResult for
        select * from DISTRIBUTION 
        join DistributionTypes using(distTypeId)
        join RECORDLABEL using(RECLABELID)
        join MARKET using(marketId);
        
        return vResult;
    end;
    

    /*
    Gets all the contributors of the database
    returns a cursor 
    */
    function getAllContributorsFromTable
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin

        open vResult for
        select * from contributor;
        
        return vResult;
    end;
    

    /*
    Gets all activity logs between start date and end date

    param pStartDate
    param pEndDate
    returns a cursor 
    */
    function getActivityLogs(pStartDate in activityLog.dateTime%type, pEndDate in activityLog.dateTime%type, pIsValid out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin

        if pStartDate >= pEndDate then 
            pIsValid := 0;
        else 
            pIsValid := 1;
        end if;

        open vResult for
        select * from ActivityLog where dateTime >= pStartDate and dateTime <= pEndDate;
        
        return vResult;
    end;
    

    /*
    Gets the details of a performance in the performance table

    param pPerformId (performId of the performance)
    param pIsExisting (1 or 0, does the performance exist)
    returns a cursor 
    */
    function getPerformanceDetails(pPerformId in performance.performId%type, pIsExisting out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin
        
        pIsExisting := doesPerformanceExist(pPerformId);

        open vResult for
        select * from performance where performId = pPerformId;
        
        return vResult;
    end;
    

    /*
    Gets all the performance ids that a contributor played a part in

    param pContId (contId of the Contributor)
    param pIsExisting (1 or 0, does the contributor exist)
    returns a cursor 
    */
    function getPerformancesFromContributor(pContid in contributor.contid%type, pIsExisting out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin
        
        pIsExisting := doesContributorExist(pContid);

        open vResult for
        select unique performId from contributor 
        join PERFORMANCE_CONTRIBUTOR_ROLE using(contId) 
        where contId = pContId;
        
        return vResult;
    end;
    

    /*
    Gets all the immediate components metadata

    param pPerformId (performId of the performance)
    param pIsExisting (1 or 0, does the performance exist)
    returns a cursor 
    */
    function getAllComponents(pPerformId in performance.performId%type, pIsExisting out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin
        
        pIsExisting := doesPerformanceExist(pPerformId);

        open vResult for
        select * from performance_component where compilationId = pPerformId;
        
        return vResult;
    end;
    

    /*
    Gets all the contributors given a performance

    param pPerformId (performId of the performance)
    param pIsExisting (1 or 0, does the performance exist)
    returns a cursor 
    */
    function getAllContributors(pPerformId in performance.performId%type, pIsExisting out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin
        
        pIsExisting := doesPerformanceExist(pPerformId);

        open vResult for
        select unique contId, firstname, lastname from PERFORMANCE_CONTRIBUTOR_ROLE 
        join CONTRIBUTOR using(CONTID)
        where performid = pPerformId;
        
        return vResult;
    end;
    

    /*
    Gets all roles given a performance and contributor

    param pPerformId (performId of the performance)
    param pContId (contId of the contributor)
    param pIsExisting (1 or 0, does the performance or contributor exist)
    returns a cursor 
    */
    function getContributorRolesFromPerformance(
        pPerformId in performance.performId%type, 
        pContId in CONTRIBUTOR.contId%type, 
        pIsExisting out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin
        
        if doesPerformanceExist(pPerformId) = 0 or doesContributorExist(pContId) = 0 then
            pIsExisting := 0;
        else
            pIsExisting := 1;
        end if;

        open vResult for
        select unique roleId, roleName from PERFORMANCE_CONTRIBUTOR_ROLE 
        join role using(ROLEID)
        where performid = pPerformId and contId = pContId;
        
        return vResult;
    end;
    

    /*
    Gets all the roles of a given contributor

    not sure if we need this: we can use getAllContributorsAndRoles and then show in java the role a particular contributor

    param pContId (contId of the contributor)
    param pIsExisting (1 or 0, does the contributor exist)
    returns a cursor 
    */
    function getAllRoles(pContId in CONTRIBUTOR.contId%type, pIsExisting out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin

        pIsExisting := doesContributorExist(pContId);
        
        open vResult for
        select unique roleId, ROLENAME from PERFORMANCE_CONTRIBUTOR_ROLE 
        join role using(ROLEID)
        where contid = pContId;
        
        return vResult;
    end;
    

    /*
    Gets all the details of a given distribution

    param pDistId (distId of the distribution)
    param pIsExisting (1 or 0, does the distribution exist)
    returns a cursor 
    */
    function getDistributionDetails(pDistId in DISTRIBUTION.distId%type, pIsExisting out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin

        pIsExisting := doesDistributionExist(pDistId);
        
        open vResult for
        select * from DISTRIBUTION 
        join DistributionTypes using(distTypeId)
        join RECORDLABEL using(RECLABELID)
        join MARKET using(marketId)
        where distId = pDistId;
        
        return vResult;
    end;
    

    /*
    Gets all the distributions of a given performance

    NOT USED IN OUR PROGRAM

    param pPerformId (performId of the performance)
    param pIsExisting (1 or 0, does the performance exist)
    returns a cursor 
    */
    function getDistributionsFromPerformance(pPerformId in PERFORMANCE.performId%type, pIsExisting out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin

        pIsExisting := doesPerformanceExist(pPerformId);
        
        open vResult for
        select * from DISTRIBUTION 
        join distribution_performance using(distId)
        where performId = pPerformId;
        
        return vResult;
    end;
    

    /*
    Gets all the performances of a given distribution

    param pDistId (distId of the distribution)
    param pIsExisting (1 or 0, does the distribution exist)
    returns a cursor 
    */
    function getPerformancesFromDistribution(pDistId in Distribution.distid%type, pIsExisting out number)
        return SYS_REFCURSOR
        as
        vResult SYS_REFCURSOR;
        begin

        pIsExisting := doesDistributionExist(pDistId);
        
        open vResult for
        select * from performance 
        join distribution_performance using(performId)
        where distId = pDistId;
        
        return vResult;
    end;
    

    /*
    Given a performance it determines wether it is a compilation or not (a recording)

    param pPerformId (performId of the performance)
    param pIsExisting (1 or 0, does the performance exist)
    returns number (1 or 0)
    */
    function isCompilation(pPerformId in performance.performId%type, pIsExisting out number)
        return number
        as
        any_rows_found number;
        vResult number(1);
        begin

        pIsExisting := doesPerformanceExist(pPerformId);

        select count(*) into any_rows_found from performance_component 
        where compilationId = pPerformId;

        if any_rows_found >= 1 then
            vResult := 1;
        else
            vResult := 0;
        end if;
        
        return vResult;
    end;
    

    /*
    Given a performance it determines wether it is released or not
    checks if it is in a distribution, if the release date is not null and if the release date is after todays date

    param pPerformId (performId of the performance)
    param pIsExisting (1 or 0, does the performance exist)
    returns number (1 or 0)
    */
    function isReleased(pPerformId in performance.performId%type, pIsExisting out number)
        return number
        as
        any_rows_found number;
        vResult number(1) := 0;
        begin

        pIsExisting := doesPerformanceExist(pPerformId);

        select count(*) into any_rows_found from DISTRIBUTION 
        join distribution_performance using(distId)
        where performId = pPerformId;

        if any_rows_found >= 1 then
            for vRow in (select * from DISTRIBUTION 
                            join distribution_performance using(distId)
                            where performId = pPerformId)
            LOOP
                dbms_output.put_line(vRow.releaseDate);
                if vRow.releaseDate is not null and vRow.releaseDate <= current_date THEN
                    vResult := 1;
                end if;
            end LOOP;
        end if;

        return vResult;
    end;
    

    /*
    Given a performance it checks if it exists

    param pPerformId (performId of the performance)
    returns number (1 or 0)
    */
    function doesPerformanceExist(pPerformId in performance.performId%type)
        return number
        as
        any_rows_found number(1);
        vResult number(1);
        begin

        select count(*) into any_rows_found from performance 
        where performId = pPerformId;

        if any_rows_found >= 1 then
            vResult := 1;
        else
            vResult := 0;
        end if;
        
        return vResult;
    end;
    

    /*
    Given a distribution it checks if it exists

    param pDistId (distId of the distribution)
    returns number (1 or 0)
    */
    function doesDistributionExist(pDistId in distribution.distId%type)
        return number
        as
        any_rows_found number(1);
        vResult number(1);
        begin

        select count(*) into any_rows_found from distribution 
        where distId = pDistId;

        if any_rows_found >= 1 then
            vResult := 1;
        else
            vResult := 0;
        end if;
        
        return vResult;
    end;
    

    /*
    Given a contributor it checks if it exists

    param pContId (contId of the contributor)
    returns number (1 or 0)
    */
    function doesContributorExist(pContId in contributor.contId%type)
        return number
        as
        any_rows_found number(1);
        vResult number(1);
        begin

        select count(*) into any_rows_found from contributor 
        where contId = pContId;

        if any_rows_found >= 1 then
            vResult := 1;
        else
            vResult := 0;
        end if;
        
        return vResult;
    end;
    

end GetData;
/


