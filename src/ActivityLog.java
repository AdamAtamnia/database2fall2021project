

import java.sql.Date;

/**
 * Represents and activity log
 * @author Adam Atamnia
 */
public class ActivityLog {
    private int logId;
    private String userName;
    private String typeChange;
    private String changedTable;
    private String message;
    private Date dateTime;

    public ActivityLog(int logId, String userName, String typeChange, String changedTable, String message, Date dateTime){
        if (userName == null || typeChange == null || changedTable == null || message == null || dateTime == null) {
            throw new IllegalArgumentException();
        }
        
        this.logId = logId;
        this.userName = userName;
        this.typeChange = typeChange;
        this.changedTable = changedTable;
        this.message = message;
        this.dateTime = dateTime;

    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;

        if(!(obj instanceof ActivityLog)){
            return false;
        }

        ActivityLog activityLog = (ActivityLog)obj;
        if (
            this.logId == activityLog.logId &&
            this.userName.equals(activityLog.userName) &&
            this.typeChange.equals(activityLog.typeChange) &&
            this.changedTable.equals(activityLog.changedTable) &&
            this.message.equals(activityLog.message) &&
            this.dateTime.equals(activityLog.dateTime)
            ) {
            isEqual = true;
        }
        return isEqual;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Id: "+this.logId+"\n");
        builder.append("User Name: "+this.userName+"\n"); 
        builder.append("Type: "+this.typeChange+"\n");
        builder.append("Table Changed: "+this.changedTable+"\n");
        builder.append("Message: "+this.message+"\n");
        builder.append("Date: "+this.dateTime.toString()+"\n");

        return builder.toString();
    }
}
