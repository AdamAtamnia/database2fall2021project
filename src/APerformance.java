

import java.sql.Date;
import java.util.Collections;
import java.util.List;

/**
 * Abstract class representing a performance
 * @author Adam Atamnia
 */
public abstract class APerformance {
    
    private int performId;
    private String name;
    private Date creationDate;
    private String duration;
    private List<Contributor> contributors;

    public APerformance(int performId, String name, Date creationDate, String duration, List<Contributor> contributors){
        if (name == null || creationDate == null || duration == null || contributors == null) {
            throw new IllegalArgumentException();
        }

        this.performId = performId;
        this.name = name;
        this.creationDate = creationDate;
        this.duration = duration;
        this.contributors = contributors;

    }

    /**
     * Returns the name of the performance
     * @return String
     */
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        boolean isEqual = false;
        APerformance perform = (APerformance)obj;
        if (
            this.performId == perform.performId &&
            this.name.equals(perform.name) &&
            this.creationDate.equals(perform.creationDate) &&
            this.duration.equals(perform.duration) &&
            this.contributors.equals(perform.contributors)
            ) {
            isEqual = true;
        }
        return isEqual;
    }

    /**
     * Return formated string that shows a performance and only one given contributor
     * @param contId
     * @return String
     */
    public String showOnlyOneContributor(int contId) {       
        StringBuilder builder = new StringBuilder("| "+performId+" | "+name+" | "+duration+" | "+creationDate+" | ");
        int countAppearance = 0;
        for (Contributor contributor : contributors) {
            if (contributor.getContId() == contId){
                builder.append(contributor.showRolesOnly());
                countAppearance++;
            }
        } 

        if (countAppearance != 1) {
            throw new IllegalArgumentException("contId can only appear once in the list of contributors");
        }

        return builder.toString();
    }

    /**
     * Returns a formated string that only shows info that IS NOT contributors and roles
     * @return String
     * @author Adam Atamnia
     */
    public String showAllExceptContributorsAndRoles(){
        return "| "+performId+" | "+name+" | "+duration+" | "+creationDate+" |";
    }

    /**
     * Returns a formated string that only shows contributors and roles
     * @return String
     * @author Adam Atamnia
     */
    public String showContributorsAndRoles(){
        StringBuilder builder = new StringBuilder();
        builder.append("Contributors:\n");
        builder.append("| Id | Name |\n");
        for (Contributor contributor : contributors) {
            builder.append(contributor.toString()+"\n");
        } 

        return builder.toString();
    }

    /**
     * Shows all info possible
     * @return String
     * @author Adam Atamnia
     */
    @Override
    public String toString() {       
        StringBuilder builder = new StringBuilder("| "+performId+" | "+name+" | "+duration+" | "+creationDate+" | ");
        int lengthBuffer = builder.length();
        String buffer = String.join("", Collections.nCopies(lengthBuffer, " "));
        builder.append("Contributors:\n");
        builder.append(buffer+"| Id | Name |\n");
        for (Contributor contributor : contributors) {
            builder.append(buffer+contributor.toString()+"\n");
        } 

        return builder.toString();
    }

}
