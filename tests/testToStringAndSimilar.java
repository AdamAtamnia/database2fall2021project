package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.sql.SQLException;

import org.junit.Test;

import src.ActivityLog;
import src.BusinessLogic;
import src.Credentials;

public class testToStringAndSimilar {

    // This will test all the to Strings except ActivityLog
    @Test
    public void testDistributionToString(){
        String string = "| Id | Name | Type | Release Date | Record Label | Market |\n| 2 | 123Dist | Long Play | 2022-12-01 | AbcCorp | USA |\n**************************************************************\n\nPerformances of distribution 123Dist:\n| Id | Name | Duration | Creation Date |\n-------------------------Performance in 123Dist------------------------------------\n| 3 | GoodbyeComp | 00:11:00 | 2011-12-01 | Contributors:\n                                            | Id | Name |\n                                            | 6 | Zuk Red | Role(s): Voice, Editor, \n\nComponents of GoodbyeComp:\n| Offset Parent | Offset Self | Duration Used |\n---------------------------Component of GoodbyeComp-------------------------------\n| 00:00:30 | 00:00:30 | 00:01:07 |\nUsed Performance:\n| 6 | Stay away Reco | 01:04:30 | 2014-12-01 | Contributors:\n                                               | Id | Name |\n                                               | 5 | Cotta Green | Role(s): Motivational Speaker, \n\n---------------------------Component of GoodbyeComp-------------------------------\n| 00:03:30 | 00:02:30 | 00:03:00 |\nUsed Performance:\n| 7 | Do Homework Reco | 00:45:00 | 2015-12-01 | Contributors:\n                                                 | Id | Name |\n                                                 | 1 | Juju Peterson | Role(s): Sound Engineer, \n\n\n-------------------------Performance in 123Dist------------------------------------\n| 4 | HiReco | 00:05:30 | 2012-12-01 | Contributors:\n                                       | Id | Name |\n                                       | 2 | Yop Smith | Role(s): Voice, \n                                       | 3 | Kyle Wit | Role(s): Motivational Speaker, \n\n-------------------------Performance in 123Dist------------------------------------\n| 5 | Comme to me Reco | 00:07:06 | 2013-12-01 | Contributors:\n                                                 | Id | Name |\n                                                 | 4 | Kit Blue | Role(s): Voice, \n\n";
        
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            assertEquals(string, bl.getDistribution(2).toString());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    
    }

    @Test
    public void testActivityLogToString(){

        String string = "Id: 1\nUser Name: adam\nType: typeChange\nTable Changed: changedTable\nMessage: message\nDate: 2016-12-01\n";
        ActivityLog activityLog = new ActivityLog(1, "adam", "typeChange", "changedTable", "message", Date.valueOf("2016-12-1"));

        assertEquals(string, activityLog.toString());
    
    }

    // tests showAllPerformancesWithoutContributorsAndRoles() in Distribution.java and showAllExceptContributorsAndRoles() in APerformance.java
    @Test
    public void testShowAllPerformancesWithoutContributorsAndRoles(){
        String string = "Performances of distribution 123Dist:\n| Id | Name | Duration | Creation Date |\n| 3 | GoodbyeComp | 00:11:00 | 2011-12-01 |\n| 4 | HiReco | 00:05:30 | 2012-12-01 |\n| 5 | Comme to me Reco | 00:07:06 | 2013-12-01 |\n";
        
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            assertEquals(string, bl.getDistribution(2).showAllPerformancesWithoutContributorsAndRoles());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    
    }

    // tests showOnlyOneContributor() in APerformance.java and showRolesOnly() contributor.java
    @Test
    public void testShowOnlyOneContributor(){
        String string = "| 1 | HeyComp | 00:05:00 | 2017-12-01 | Role(s): Sound Engineer, ";
        
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            assertEquals(string, bl.getPerformance(1).showOnlyOneContributor(1));
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    
    }

    @Test
    public void testShowOnlyOneContributorIllegalArgument(){
        
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            bl.getPerformance(2).showOnlyOneContributor(2);
            fail("Should have caught an illegal argument");
        } catch (IllegalArgumentException e) {
            // success
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    
    }

    @Test
    public void testShowContributorsAndRoles(){
        String string = "Contributors:\n| Id | Name |\n| 2 | Yop Smith | Role(s): Voice, \n| 1 | Juju Peterson | Role(s): Sound Engineer, \n| 3 | Kyle Wit | Role(s): Motivational Speaker, \n";
        
        Credentials credentials = new Credentials();
        BusinessLogic bl = null;
        try {
            bl = new BusinessLogic(credentials);
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        try {
            assertEquals(string, bl.getPerformance(1).showContributorsAndRoles());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected Exception");
        }
        finally{
            try {
                bl.close();
            } catch (Exception e) {
                e.printStackTrace();
                fail("Unexpected Exception");
            }
        }
    
    }
}
